/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.CentroDados;
import Model.RegistoUtilizadores;
import Model.Utilizador;
import java.util.LinkedHashMap;
import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author NOVO
 */
public class MostrarUtilizadoresInfluentesControllerTest {

    public MostrarUtilizadoresInfluentesControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    /**
     * Test of getListaUtilizadoresInfluentes method, of class MostrarUtilizadoresInfluentesController.
     */
    @Test
    public void testGetListaUtilizadoresInfluentes() {
        System.out.println("getListaUtilizadoresInfluentes");

        //Criar utilizador 1 com 6 amigos
        Utilizador u1 = new Utilizador("quim", "email@isep.ipp.pt");
        u1.getListaAmigos().addUtilizador(new Utilizador());
        u1.getListaAmigos().addUtilizador(new Utilizador());
        u1.getListaAmigos().addUtilizador(new Utilizador());
        u1.getListaAmigos().addUtilizador(new Utilizador());
        u1.getListaAmigos().addUtilizador(new Utilizador());
        u1.getListaAmigos().addUtilizador(new Utilizador());

        //criar utilizador u2 com 3 amigos
        Utilizador u2 = new Utilizador("diog", "email@isep.ipp.pt");
        u2.getListaAmigos().addUtilizador(new Utilizador());
        u2.getListaAmigos().addUtilizador(new Utilizador());
        u2.getListaAmigos().addUtilizador(new Utilizador());

        //Criar utilizador3 com 4 amigos
        Utilizador u3 = new Utilizador("diog02", "email@isep.ipp.pt");
        u3.getListaAmigos().addUtilizador(new Utilizador());
        u3.getListaAmigos().addUtilizador(new Utilizador());
        u3.getListaAmigos().addUtilizador(new Utilizador());
        u3.getListaAmigos().addUtilizador(new Utilizador());

        //Criar utilizador u4 com 3 amigos
        Utilizador u4 = new Utilizador("diog03", "email@isep.ipp.pt");
        u4.getListaAmigos().addUtilizador(new Utilizador());
        u4.getListaAmigos().addUtilizador(new Utilizador());
        u4.getListaAmigos().addUtilizador(new Utilizador());

        RegistoUtilizadores ru = new RegistoUtilizadores();

        //Adicionar utilizadores ao registo
        ru.addUtilizador(u4);
        ru.addUtilizador(u3);
        ru.addUtilizador(u1);
        ru.addUtilizador(u2);
        
        CentroDados cd = new CentroDados();
        cd.setRegistoUtilizadores(ru);

        MostrarUtilizadoresInfluentesController instance = new MostrarUtilizadoresInfluentesController(cd);

        Map<Utilizador, Integer> expResult = new LinkedHashMap<>();

        //Certo
        expResult.put(u1, 6);
        expResult.put(u3, 4);
        expResult.put(u4, 3);
        expResult.put(u2, 3);

        //Errado 
//        expResult.put(u2, 3);
//        expResult.put(u3, 4);
//        expResult.put(u4, 3);
//        expResult.put(u1, 6);
        Map<Utilizador, Integer> result = instance.getListaUtilizadoresInfluentes();
        if (!result.keySet().toString().equals(expResult.keySet().toString())) {
            assertTrue(false);
        }
        assertEquals(expResult, result);
    }
}
