/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.CentroDados;
import Model.RegistoUtilizadores;
import Model.Utilizador;
import java.util.LinkedList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author NOVO
 */
public class CriarAmizadeControllerTest {

    public CriarAmizadeControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of addAmigo method, of class CriarAmizadeController.
     */
    @Test
    public void testAddAmigo() {
        System.out.println("addAmigo");
        String nickname = "Este";
        String nickname2 = "Amigo";
        Utilizador u = new Utilizador("Este", "mail@gmail.com");
        Utilizador u1 = new Utilizador(nickname, "a@gmail.com");
        Utilizador u2 = new Utilizador(nickname2, "b@gmail.com");
        List<Utilizador> lu = new LinkedList<>();
        lu.add(u);
        lu.add(u1);
        lu.add(u2);
        RegistoUtilizadores ru = new RegistoUtilizadores(lu);
        CentroDados cd = new CentroDados();
        cd.setRegistoUtilizadores(ru);
        u.getListaAmigos().addUtilizador(u2);
        CriarAmizadeController instance = new CriarAmizadeController(cd, u);
        boolean result = instance.addAmigo(nickname);
        boolean result2 = instance.addAmigo(nickname2);
        assertTrue(result);
        assertFalse(result2);
    }

}
