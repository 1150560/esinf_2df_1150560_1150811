/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.CentroDados;
import Model.Utilizador;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author NOVO
 */
public class CriarUtilizadorControllerTest {

    public CriarUtilizadorControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    CentroDados cd = new CentroDados();
    CriarUtilizadorController instance = new CriarUtilizadorController(cd);

    /**
     * Test of novoUtilizador method, of class CriarUtilizadorController.
     */
    @Test
    public void testNovoUtilizador() {
        System.out.println("novoUtilizador");
        instance.novoUtilizador();
        boolean esperado = instance.user.equals(new Utilizador());
        assertTrue(esperado);
    }

    /**
     * Test of setDados method, of class CriarUtilizadorController.
     */
    @Test
    public void testSetDados() {
        System.out.println("setDados");
        String nickname = "NovoNick";
        String email = "novo@gmail.com";
        instance.novoUtilizador();
        instance.setDados(nickname, email);
        Utilizador u = new Utilizador(nickname, email);
        boolean esperado = instance.user.equals(u);
        assertTrue(esperado);

    }

    /**
     * Test of registaUtilizador method, of class CriarUtilizadorController.
     */
    @Test
    public void testRegistaUtilizador() {
        System.out.println("registaUtilizador");
        instance.novoUtilizador();
        String nickname = "NovoNick";
        String email = "novo@gmail.com";
        instance.setDados(nickname, email);
        assertTrue(instance.registaUtilizador());
        assertFalse(instance.registaUtilizador());
    }
}
