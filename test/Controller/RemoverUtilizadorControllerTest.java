/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.CentroDados;
import Model.RegistoUtilizadores;
import Model.Utilizador;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author w8
 */
public class RemoverUtilizadorControllerTest {

    /**
     * Test of removerUtilizador method, of class RemoverUtilizadorController.
     */
    @Test
    public void testRemoverUtilizador() {
        System.out.println("removerUtilizador");

        CentroDados cd = new CentroDados();
        Utilizador u = new Utilizador("quim", "1150811@isep.ipp.pt");
        Utilizador u2 = new Utilizador("Diogo", "email@isep.ipp.pt");
        Utilizador u3 = new Utilizador();

        //Adicionar à lista de amigos de u2 e u3, u
        u2.getListaAmigos().addUtilizador(u);
        u3.getListaAmigos().addUtilizador(u);

        //Adicionar ao utilizador os amigos u2 e u3
        u.getListaAmigos().addUtilizador(u2);
        u.getListaAmigos().addUtilizador(u3);

        //Adicionar ao registo de utilizadores os utilizadores u, u2, u3
        RegistoUtilizadores ru = new RegistoUtilizadores();
        ru.addUtilizador(u);
        ru.addUtilizador(u2);
        ru.addUtilizador(u3);

        //Adicionar ao centro de dados o registo de utilizadores
        cd.setRegistoUtilizadores(ru);

        RemoverUtilizadorController instance = new RemoverUtilizadorController(cd);
        boolean result = instance.removerUtilizador(u);
        assertTrue(result);

        //Verifica se u ainda existe na lista de amigos de u2
        for (Utilizador u4 : u2.getListaAmigos().getListaUtilizadores()) {
            if (u4.equals(u)) {
                assertFalse(true);
            }
        }

        //Verifica se u ainda existe na lista de amigos de u3
        for (Utilizador u4 : u3.getListaAmigos().getListaUtilizadores()) {
            if (u4.equals(u)) {
                assertFalse(true);
            }
        }
    }

}
