/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.CentroDados;
import Model.Cidade;
import Model.RegistoCidades;
import Model.Utilizador;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author NOVO
 */
public class MostrarCidadeControllerTest {
    
    public MostrarCidadeControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}

    /**
     * Test of getListaCidades method, of class MostrarCidadeController.
     */
    @Test
    public void testGetListaCidades() {
        System.out.println("getListaCidades");
        List<Cidade> lc = new LinkedList<>();
        Cidade c1 = new Cidade("cidade1", 10, 15, 20);
        Cidade c2 = new Cidade("cidade2", 10, 15, 20);
        Cidade c3 = new Cidade("cidade3", 10, 15, 20);
        Utilizador u1 = new Utilizador("nome1", "e1@gmail.com");
        u1.getListaCidades().put(c1, 20);
        Utilizador u2 = new Utilizador("nome2", "e2@gmail.com");
        u2.getListaCidades().put(c2, 30);
        Utilizador u3 = new Utilizador("nome3", "e3@gmail.com");
        u3.getListaCidades().put(c3, 10);
        c1.setMayor(u1);
        c2.setMayor(u2);
        c3.setMayor(u3);
        lc.add(c1);
        lc.add(c2);
        lc.add(c3);
        RegistoCidades rc = new RegistoCidades(lc);
        CentroDados cd = new CentroDados();
        cd.setRegistoCidades(rc);
        Map<Cidade, Integer> expResult = new LinkedHashMap<>();
        expResult.put(c2, 30);
        expResult.put(c1, 20);
        expResult.put(c3, 10);
        MostrarCidadeController instance = new MostrarCidadeController(cd);
        Map<Cidade, Integer> result = instance.getListaCidades();
        assertEquals(expResult, result);
    }
}
