/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.CentroDados;
import Model.Cidade;
import Model.RegistoCidades;
import Model.RegistoUtilizadores;
import Model.Utilizador;
import java.util.LinkedList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author NOVO
 */
public class MostrarAmigosControllerTest {

    public MostrarAmigosControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getListaCidades method, of class MostrarAmigosController.
     */
    @Test
    public void testGetListaCidades() {
        System.out.println("getListaCidades");

        Utilizador u = new Utilizador("Diogo", "1150560@isep.ipp.pt");

        //Criar 2 cidades
        Cidade c1 = new Cidade("c1", 10, 20, 30);
        Cidade c2 = new Cidade("c2", 20, 30, 40);
        
        RegistoCidades rc = new RegistoCidades();
        
        //Adicionar as cidades ao registo
        rc.addCidade(c1);
        rc.addCidade(c2);
        
        CentroDados cd = new CentroDados();
        cd.setRegistoCidades(rc);
       
        MostrarAmigosController instance = new MostrarAmigosController(cd, u);
        List<Cidade> expResult = new LinkedList<>();
        
        //Adicionar as cidades ao resultado esperado
        expResult.add(c1);
        expResult.add(c2);
        
        List<Cidade> result = instance.getListaCidades();
        assertArrayEquals(expResult.toArray(), result.toArray());
    }

    /**
     * Test of getListaAmigosLocalizacao method, of class MostrarAmigosController.
     */
    @Test
    public void testGetListaAmigosLocalizacao() {
        System.out.println("getListaAmigosLocalizacao");

        //Criação do Utilizador principal
        Utilizador u = new Utilizador("Diogo", "1150560@isep.ipp.pt");

        //Criação de 4 utilizadores
        Utilizador u2 = new Utilizador("u2", "email2@gmail.com");
        Utilizador u3 = new Utilizador("u3", "email2@gmail.com");
        Utilizador u4 = new Utilizador("u4", "email2@gmail.com");
        Utilizador u5 = new Utilizador("u5", "email2@gmail.com");

        //Criação de 2 cidades
        Cidade c = new Cidade("Porto", 10, 20, 30);
        Cidade c2 = new Cidade("Lisboa", 20, 30, 40);

        //Adicionar 2 utilizadores à cidade pretendida
        u3.setCidadeAtual(c);
        u4.setCidadeAtual(c);

        //Adicionar 2 utilizadores à outra cidade
        u2.setCidadeAtual(c2);
        u5.setCidadeAtual(c2);

        RegistoUtilizadores la = new RegistoUtilizadores();

        //Adicionar os utilizadores à lista de amigos
        la.addUtilizador(u2);
        la.addUtilizador(u3);
        la.addUtilizador(u4);
        la.addUtilizador(u5);

        u.setListaAmigos(la);

        MostrarAmigosController instance = new MostrarAmigosController(new CentroDados(), u);
        List<Utilizador> expResult = new LinkedList<>();

        //Adicionar os utilizadores certos ao resultado esperado
        expResult.add(u3);
        expResult.add(u4);

        List<Utilizador> result = instance.getListaAmigosLocalizacao(c);
        assertArrayEquals(expResult.toArray(), result.toArray());
    }

}
