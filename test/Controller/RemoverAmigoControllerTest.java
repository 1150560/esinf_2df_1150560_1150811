/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Utilizador;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author w8
 */
public class RemoverAmigoControllerTest {
    
    public RemoverAmigoControllerTest() {
    }
    /**
     * Test of getListaAmigos method, of class RemoverAmigoController.
     */
    @Test
    public void testGetListaAmigos() {
        System.out.println("getListaAmigos");
        Utilizador u = new Utilizador("Quim", "1150811@isep.ipp.pt");
        Utilizador u2 = new Utilizador("Diogo", "1150560@isep.ipp.pt" );
        
        //Adiciona à lista de amigos os utilizadores
        u.getListaAmigos().addUtilizador(u2);
        u2.getListaAmigos().addUtilizador(u);
        RemoverAmigoController instance = new RemoverAmigoController(u);
        
        instance.getListaAmigos();
        //Verfica se atualizou a variavel utilizador
        boolean result = instance.u.equals(u);
        //Verifica se a lista foi atualizada
        List lista1 = u.getListaAmigos().getListaUtilizadores();
        List lista2 = instance.listaAmigos.getListaUtilizadores();
        assertTrue(result);
        assertArrayEquals(lista1.toArray(), lista2.toArray());
    }

    /**
     * Test of removeAmigos method, of class RemoverAmigoController.
     */
    @Test
    public void testRemoveAmigos() {
        System.out.println("removeAmigos");
        Utilizador u = new Utilizador("Quim", "1150811@isep.ipp.pt");
        Utilizador u2 = new Utilizador("Diogo", "1150560@isep.ipp.pt" );
        boolean esperado = false;
        //Adiciona à lista de amigos os utilizadores
        u.getListaAmigos().addUtilizador(u2);
        u2.getListaAmigos().addUtilizador(u);
        RemoverAmigoController instance = new RemoverAmigoController(u);
        
        instance.getListaAmigos();
        boolean result = instance.removeAmigos(u2);
        assertTrue(result);
        
        //Verifica se removeu mesmo na lista de amigos
        Utilizador u3 = u.getListaAmigos().getUtilizador("Diogo");
        Utilizador u4 = u2.getListaAmigos().getUtilizador("Quim");
        if(u3 == null && u4 == null){
           esperado = true;
        }
        assertTrue(esperado);
    }
    
}
