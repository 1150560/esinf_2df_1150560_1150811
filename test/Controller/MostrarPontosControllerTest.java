/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Cidade;
import Model.Utilizador;
import java.util.LinkedList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author NOVO
 */
public class MostrarPontosControllerTest {

    public MostrarPontosControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getPontosTotais method, of class MostrarPontosController.
     */
    @Test
    public void testGetPontosTotais() {
        System.out.println("getPontosTotais");
        Utilizador u = new Utilizador("nome1", "email1@gmail.com");
        int pontosTotais = 72;
        u.setPontosTotais(pontosTotais);
        MostrarPontosController instance = new MostrarPontosController(u);
        int expResult = 72;
        int result = instance.getPontosTotais();
        assertEquals(expResult, result);
    }

    /**
     * Test of getListaCidades method, of class MostrarPontosController.
     */
    @Test
    public void testGetListaCheckIn() {
        System.out.println("getListaCidades");
        Utilizador u = new Utilizador("nome1", "email1@gmail.com");
        Cidade c1 = new Cidade("c1", 1, 2, 3);
        Cidade c2 = new Cidade("c2", 11, 12, 13);
        Cidade c3 = new Cidade("c3", 21, 22, 23);
        LinkedList lc = new LinkedList<>();
        lc.add(c1);
        lc.add(c2);
        lc.add(c1);
        lc.add(c3);
        lc.add(c1);
        u.setListaCheckIn(lc);
        MostrarPontosController instance = new MostrarPontosController(u);
        LinkedList expResult = new LinkedList<>();
        expResult.add(c1);
        expResult.add(c2);
        expResult.add(c1);
        expResult.add(c3);
        expResult.add(c1);

        LinkedList result = instance.getListaCheckIn();
        assertArrayEquals(expResult.toArray(), result.toArray());
    }

}
