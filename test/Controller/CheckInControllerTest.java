/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.CentroDados;
import Model.Cidade;
import Model.RegistoCidades;
import Model.Utilizador;
import java.util.LinkedList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author NOVO
 */
public class CheckInControllerTest {

    public CheckInControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getListaCidades method, of class CheckInController.
     */
    @Test
    public void testGetListaCidades() {
        System.out.println("getListaCidades");
        Cidade c1 = new Cidade("c1", 1, 2, 3);
        Cidade c2 = new Cidade("c2", 2, 3, 4);
        List<Cidade> lc = new LinkedList<>();
        lc.add(c1);
        lc.add(c2);
        RegistoCidades rc = new RegistoCidades(lc);
        CentroDados cd = new CentroDados();
        cd.setRegistoCidades(rc);
        Utilizador u1 = new Utilizador("u1", "email1@gmail.com");
        u1.setCidadeAtual(c2);
        CheckInController instance = new CheckInController(cd, u1);
        RegistoCidades expResult = rc;
        RegistoCidades result = instance.getListaCidades();
        assertEquals(expResult, result);
    }

    /**
     * Test of isCidadeAtual method, of class CheckInController.
     */
    @Test
    public void testIsCidadeAtual() {
        System.out.println("isCidadeAtual");

        //Fazer o método anterior para atualizar a cidade atual
        Cidade c1 = new Cidade("c1", 1, 2, 3);
        Cidade c2 = new Cidade("c2", 2, 3, 4);
        List<Cidade> lc = new LinkedList<>();
        lc.add(c1);
        lc.add(c2);
        RegistoCidades rc = new RegistoCidades(lc);
        CentroDados cd = new CentroDados();
        cd.setRegistoCidades(rc);
        Utilizador u1 = new Utilizador("u1", "email1@gmail.com");
        u1.setCidadeAtual(c2);
        CheckInController instance = new CheckInController(cd, u1);
        instance.getListaCidades();

        boolean esperado = instance.isCidadeAtual(c2);
        boolean esperado2 = instance.isCidadeAtual(c1);
        assertTrue(esperado);
        assertFalse(esperado2);
    }

}
