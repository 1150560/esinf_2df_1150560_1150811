/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.CentroDados;
import Model.Cidade;
import Model.RegistoCidades;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author w8
 */
public class LerCidadesControllerTest {
    
    public LerCidadesControllerTest() {
    }
    
    /**
     * Test of registaCidades method, of class LerCidadesController.
     */
    @Test
    public void testRegistaCidades() {
        System.out.println("registaCidades");
        String nomeFicheiro =  "cities10";
        CentroDados cd = new CentroDados();
        
        RegistoCidades r = new RegistoCidades();
        r.addCidade(new Cidade("city0", 28, 41.243345, -8.674084));
        r.addCidade(new Cidade("city1", 72, 41.237364, -8.846746));
        r.addCidade(new Cidade("city2", 81, 40.519841, -8.085113));
        r.addCidade(new Cidade("city3", 42, 41.118700, -8.589700));
        r.addCidade(new Cidade("city4", 64, 41.467407, -8.964340));
        r.addCidade(new Cidade("city5", 74, 41.337408, -8.291943));
        r.addCidade(new Cidade("city6", 80, 41.314965, -8.423371));
        r.addCidade(new Cidade("city7", 11, 40.822244, -8.794953));
        r.addCidade(new Cidade("city8", 7, 40.781886, -8.697502));
        r.addCidade(new Cidade("city9", 65, 40.851360, -8.136585));
        
        LerCidadesController lc = new LerCidadesController(cd);
        
        lc.registaCidades(nomeFicheiro);
        RegistoCidades result = cd.getRegistoCidades();
        assertEquals(result.toString(), r.toString());
        
    }
    
}