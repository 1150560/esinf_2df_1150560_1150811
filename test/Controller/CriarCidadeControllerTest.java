/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.CentroDados;
import Model.Cidade;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author w8
 */
public class CriarCidadeControllerTest {
    
    public CriarCidadeControllerTest() {
    }
    

    /**
     * Test of novaCidade method, of class CriarCidadeController.
     */
    @Test
    public void testNovaCidade() {
        System.out.println("novaCidade");
        CriarCidadeController instance = new CriarCidadeController(new CentroDados());
        instance.novaCidade();
        Cidade cidade = new Cidade();
        assertTrue(cidade.equals(instance.c));
    }

    /**
     * Test of SetDados method, of class CriarCidadeController.
     */
    @Test
    public void testSetDados() {
        System.out.println("SetDados");
        String nome = "nome";
        int pontos = 10;
        double latitude = 10.0;
        double longitude = 10.0;
        CriarCidadeController instance = new CriarCidadeController(new CentroDados());
        instance.novaCidade();
        instance.SetDados(nome, pontos, latitude, longitude);
        Cidade cidade = new Cidade(nome, pontos, latitude, longitude);
        assertTrue(cidade.equals(instance.c));
    }

    
    /**
     * Test of SetDados method, of class CriarCidadeController.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testSetDados2() {
        System.out.println("SetDados2");
        String nome = "";
        int pontos = 10;
        double latitude = 10.0;
        double longitude = 10.0;
        CriarCidadeController instance = new CriarCidadeController(new CentroDados());
        instance.novaCidade();
        instance.SetDados(nome, pontos, latitude, longitude);
        Cidade cidade = new Cidade(nome, pontos, latitude, longitude);
        assertTrue(cidade.equals(instance.c));
    }

    /**
     * Test of registaCidade method, of class CriarCidadeController.
     */
    @Test
    public void testRegistaCidade() {
        System.out.println("registaCidade");
        CriarCidadeController instance = new CriarCidadeController(new CentroDados());
        instance.novaCidade();
        boolean result = instance.registaCidade();
        assertTrue(result);
        boolean result2 = instance.registaCidade();
        assertFalse(result2);
    }
    
}
