/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author NOVO
 */
public class MayorTest {

    public MayorTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getUtilizador method, of class Mayor.
     */
    @Test
    public void testGetUtilizador() {
        System.out.println("getUtilizador");
        Mayor instance = new Mayor();
        Utilizador expResult = new Utilizador();
        Utilizador result = instance.getUtilizador();
        boolean esperado = expResult.equals(result);
        assertTrue(esperado);

    }

    /**
     * Test of getCidade method, of class Mayor.
     */
    @Test
    public void testGetCidade() {
        System.out.println("getCidade");
        Mayor instance = new Mayor();
        Cidade expResult = new Cidade();
        Cidade result = instance.getCidade();
        boolean esperado = expResult.equals(result);
        assertTrue(esperado);

    }

    /**
     * Test of getPontos method, of class Mayor.
     */
    @Test
    public void testGetPontos() {
        System.out.println("getPontos");
        Mayor instance = new Mayor();
        int expResult = 0;
        int result = instance.getPontos();
        assertEquals(expResult, result);
    }

    /**
     * Test of setUtilizador method, of class Mayor.
     */
    @Test
    public void testSetUtilizador() {
        System.out.println("setUtilizador");
        Utilizador utilizador = new Utilizador("Teste1", "email1@gmail.com");
        Mayor instance = new Mayor();
        instance.setUtilizador(utilizador);
        boolean esperado = utilizador.equals(instance.getUtilizador());
        assertTrue(esperado);
    }

    /**
     * Test of setCidade method, of class Mayor.
     */
    @Test
    public void testSetCidade() {
        System.out.println("setCidade");
        Cidade cidade = new Cidade("cidade1", 1, 1, 1);
        Mayor instance = new Mayor();
        instance.setCidade(cidade);
        boolean esperado = cidade.equals(instance.getCidade());
        assertTrue(esperado);
    }

    /**
     * Test of setPontos method, of class Mayor.
     */
    @Test
    public void testSetPontos() {
        System.out.println("setPontos");
        int pontos = 50;
        Mayor instance = new Mayor();
        instance.setPontos(pontos);
        assertEquals(pontos, instance.getPontos());
    }

    /**
     * Test of compareTo method, of class Mayor.
     */
    @Test
    public void testCompareTo() {
        System.out.println("compareTo");
        Mayor t = new Mayor(new Utilizador("a", "a@gmail.com"), new Cidade("c1", 1, 1, 1), 40);
        Mayor instance = new Mayor(new Utilizador("b", "b@gmail.com"), new Cidade("c2", 2, 2, 2), 50);
        int expResult = 10;
        int result = instance.compareTo(t);
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Mayor.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Mayor instance = new Mayor();
        String expResult = "[0]";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
}
