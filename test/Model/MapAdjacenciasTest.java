/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Graph.Graph;
import Graph.Vertex;
import java.util.LinkedList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author NOVO
 */
public class MapAdjacenciasTest {

    private CentroDados cd;
    private MapAdjacencias instance;

    public MapAdjacenciasTest() {
        cd = new CentroDados();
        instance = new MapAdjacencias();
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {

        //Ler cidades
        LerFicheiro ler = new LerFicheiro(cd.getRegistoCidades(), "cities10");
        ler.carregarCidades();

        //Ler os utilizadores
        LerFicheiro ler3 = new LerFicheiro(cd.getRegistoUtilizadores(), cd.getRegistoCidades(), "users10");
        ler3.carregarUtilizadores();
    }

    @After
    public void tearDown() {
    }

    /**
     * System.out.println("getMap"); Test of getMap method, of class MapAdjacencias.
     */
    @Test
    public void testGetMap() {
        System.out.println("getMap");
        Graph<Utilizador, Vertex> expResult = new Graph<>(false);
        Graph<Utilizador, Vertex> result = instance.getMap();
        assertEquals(expResult, result);
    }

    /**
     * Test of setMap method, of class MapAdjacencias.
     */
    @Test
    public void testSetMap() {
        System.out.println("setMap");
        Graph<Utilizador, Vertex> map = new Graph<>(false);
        instance.setMap(map);
        assertEquals(map, instance.getMap());
    }

    /**
     * Test of mapAdjacencias method, of class MapAdjacencias.
     */
    @Test
    public void testMapAdjacencias() {
        System.out.println("mapAdjacencias");

        instance.mapAdjacencias(cd.getRegistoUtilizadores().getListaUtilizadores());

        //resultado esperado
        Graph<Utilizador, Vertex> esperado = new Graph(false);

        Utilizador u0 = cd.getRegistoUtilizadores().getUtilizador("nick0");
        Utilizador u1 = cd.getRegistoUtilizadores().getUtilizador("nick1");
        Utilizador u2 = cd.getRegistoUtilizadores().getUtilizador("nick2");
        Utilizador u3 = cd.getRegistoUtilizadores().getUtilizador("nick3");
        Utilizador u4 = cd.getRegistoUtilizadores().getUtilizador("nick4");
        Utilizador u5 = cd.getRegistoUtilizadores().getUtilizador("nick5");
        Utilizador u6 = cd.getRegistoUtilizadores().getUtilizador("nick6");
        Utilizador u7 = cd.getRegistoUtilizadores().getUtilizador("nick7");
        Utilizador u8 = cd.getRegistoUtilizadores().getUtilizador("nick8");
        Utilizador u9 = cd.getRegistoUtilizadores().getUtilizador("nick9");

        esperado.insertEdge(u0, u7, null, 1);
        esperado.insertEdge(u0, u4, null, 1);
        esperado.insertEdge(u0, u3, null, 1);
        esperado.insertEdge(u1, u6, null, 1);
        esperado.insertEdge(u1, u2, null, 1);
        esperado.insertEdge(u1, u8, null, 1);
        esperado.insertEdge(u2, u1, null, 1);
        esperado.insertEdge(u2, u6, null, 1);
        esperado.insertEdge(u2, u7, null, 1);
        esperado.insertEdge(u2, u8, null, 1);
        esperado.insertEdge(u3, u8, null, 1);
        esperado.insertEdge(u3, u7, null, 1);
        esperado.insertEdge(u3, u6, null, 1);
        esperado.insertEdge(u3, u5, null, 1);
        esperado.insertEdge(u4, u9, null, 1);
        esperado.insertEdge(u5, u8, null, 1);
        esperado.insertEdge(u5, u7, null, 1);
        esperado.insertEdge(u5, u6, null, 1);
        esperado.insertEdge(u5, u9, null, 1);
        esperado.insertEdge(u8, u9, null, 1);

        assertEquals(esperado, instance.getMap());
    }

    /**
     * Test of amigosDistancia method, of class MapAdjacencias.
     */
    @Test
    public void testAmigosDistancia() {
        System.out.println("amigosDistancia");
        instance.mapAdjacencias(cd.getRegistoUtilizadores().getListaUtilizadores());
        Object[] lista = instance.getMap().allkeyVerts();
        Utilizador principal = (Utilizador) lista[0];

        int d = 2;
        List<Utilizador> expResult = new LinkedList<>();
        expResult.add((Utilizador) lista[1]);
        expResult.add((Utilizador) lista[2]);
        expResult.add((Utilizador) lista[3]);
        expResult.add((Utilizador) lista[5]);
        expResult.add((Utilizador) lista[6]);
        expResult.add((Utilizador) lista[7]);
        expResult.add((Utilizador) lista[8]);
        expResult.add((Utilizador) lista[9]);

        List<Utilizador> result = instance.amigosDistancia(principal, d);
        assertArrayEquals(expResult.toArray(), result.toArray());

    }

    /**
     * Test of dista2Utilizadores method, of class MapAdjacencias.
     */
    @Test
    public void testDistancia2Utilizadores() {
        System.out.println("distancia2Utilizadores");
        instance.mapAdjacencias(cd.getRegistoUtilizadores().getListaUtilizadores());
        Object[] lista = instance.getMap().allkeyVerts();
        Utilizador u1 = (Utilizador) lista[0];
        Utilizador u2 = (Utilizador) lista[4];
        double expResult = 2.0;
        double result = instance.distancia2Utilizadores(u1, u2);
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of utilizadoresMaisInfluentes method, of class MapAdjacencias.
     */
    @Test
    public void testUtilizadoresMaisInfluentes() {
        System.out.println("utilizadoresMaisInfluentes");
        instance.mapAdjacencias(cd.getRegistoUtilizadores().getListaUtilizadores());
        List<Utilizador> expResult = new LinkedList<>();
        List<Utilizador> result = instance.utilizadoresMaisInfluentes();

        Object[] lista = instance.getMap().allkeyVerts();

        expResult.add((Utilizador) lista[1]);
        expResult.add((Utilizador) lista[3]);
        expResult.add((Utilizador) lista[7]);
        expResult.add((Utilizador) lista[8]);
        expResult.add((Utilizador) lista[9]);
        assertArrayEquals(expResult.toArray(), result.toArray());

    }

    /**
     * Test of listaAmigos method, of class MapAdjacencias.
     */
    @Test
    public void testListaAmigos() {
        System.out.println("listaAmigos");
        instance.mapAdjacencias(cd.getRegistoUtilizadores().getListaUtilizadores());
        Object[] lista = instance.getMap().allkeyVerts();
        Utilizador u = (Utilizador) lista[0];
        List<Utilizador> expResult = new LinkedList<>();
        expResult.add((Utilizador) lista[1]);
        expResult.add((Utilizador) lista[2]);
        expResult.add((Utilizador) lista[3]);
        List<Utilizador> result = instance.listaAmigos(u);
        boolean resultado = result.containsAll(expResult);
        boolean resultado2 = expResult.containsAll(result);
        assertTrue(resultado && resultado2);
    }

}
