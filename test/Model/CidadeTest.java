/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author w8
 */
public class CidadeTest {

    public CidadeTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getNome method, of class Cidade.
     */
    @Test
    public void testGetNome() {
        System.out.println("getNome");
        Cidade instance = new Cidade();
        String expResult = "sem nome";
        String result = instance.getNome();
        assertEquals(expResult, result);
    }

    /**
     * Test of getPontos method, of class Cidade.
     */
    @Test
    public void testGetPontos() {
        System.out.println("getPontos");
        Cidade instance = new Cidade();
        int expResult = 0;
        int result = instance.getPontos();
        assertEquals(expResult, result);
    }

    /**
     * Test of getLatitude method, of class Cidade.
     */
    @Test
    public void testGetLatitude() {
        System.out.println("getLatitude");
        Cidade instance = new Cidade();
        double expResult = 0.0;
        double result = instance.getLatitude();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getLongitude method, of class Cidade.
     */
    @Test
    public void testGetLongitude() {
        System.out.println("getLongitude");
        Cidade instance = new Cidade();
        double expResult = 0.0;
        double result = instance.getLongitude();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of setNome method, of class Cidade.
     */
    @Test
    public void testSetNome() {
        System.out.println("setNome");
        String nome = "novo nome";
        Cidade instance = new Cidade();
        instance.setNome(nome);
        assertEquals(instance.getNome(), nome);
    }

    /**
     * Test of setNome method, of class Cidade.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testSetNome2() {
        System.out.println("setNome2");
        String nome = "";
        Cidade instance = new Cidade();
        instance.setNome(nome);
        assertEquals(instance.getNome(), nome);
    }

    /**
     * Test of setNome method, of class Cidade.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testSetNome3() {
        System.out.println("setNome2");
        String nome = null;
        Cidade instance = new Cidade();
        instance.setNome(nome);
        assertEquals(instance.getNome(), nome);
    }

    /**
     * Test of setPontos method, of class Cidade.
     */
    @Test
    public void testSetPontos() {
        System.out.println("setPontos");
        int pontos = 10;
        Cidade instance = new Cidade();
        instance.setPontos(pontos);
        assertEquals(instance.getPontos(), pontos, 0.0);
    }

    /**
     * Test of setPontos method, of class Cidade.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testSetPontos2() {
        System.out.println("setPontos2");
        int pontos = -2;
        Cidade instance = new Cidade();
        instance.setPontos(pontos);
        assertEquals(instance.getPontos(), pontos, 0.0);
    }

    /**
     * Test of setLatitude method, of class Cidade.
     */
    @Test
    public void testSetLatitude() {
        System.out.println("setLatitude");
        double latitude = 11.5;
        Cidade instance = new Cidade();
        instance.setLatitude(latitude);
        assertEquals(latitude, instance.getLatitude(), 0.0);
    }

    /**
     * Test of setLatitude method, of class Cidade.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testSetLatitude2() {
        System.out.println("setLatitude2");
        double latitude = 91;
        Cidade c = new Cidade();
        Cidade instance = new Cidade(c);
        instance.setLatitude(latitude);
        assertEquals(latitude, instance.getLatitude(), 0.0);
    }

    /**
     * Test of setLatitude method, of class Cidade.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testSetLatitude3() {
        System.out.println("setLatitude2");
        double latitude = -91;
        Cidade c = new Cidade();
        Cidade instance = new Cidade(c);
        instance.setLatitude(latitude);
        assertEquals(latitude, instance.getLatitude(), 0.0);
    }

    /**
     * Test of setLongitude method, of class Cidade.
     */
    @Test
    public void testSetLongitude() {
        System.out.println("setLongitude");
        double longitude = 13.67;
        Cidade instance = new Cidade();
        instance.setLongitude(longitude);
        assertEquals(longitude, instance.getLongitude(), 0.0);
    }

    /**
     * Test of setLongitude method, of class Cidade.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testSetLongitude2() {
        System.out.println("setLongitude2");
        double longitude = 190;
        Cidade instance = new Cidade();
        instance.setLongitude(longitude);
        assertEquals(longitude, instance.getLongitude(), 0.0);
    }

    /**
     * Test of setLongitude method, of class Cidade.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testSetLongitude3() {
        System.out.println("setLongitude2");
        double longitude = -190;
        Cidade instance = new Cidade();
        instance.setLongitude(longitude);
        assertEquals(longitude, instance.getLongitude(), 0.0);
    }

    /**
     * Test of toString method, of class Cidade.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Cidade instance = new Cidade();
        String expResult = "[sem nome]";
        String result = instance.toString();
        assertEquals(expResult, result);;
    }

    /**
     * Test of equals method, of class Cidade.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Cidade cidade = new Cidade();
        Cidade cidade2 = new Cidade("nome", 10, 0.1, 0.11);
        Cidade cidade3 = null;
        Object cidade4 = new Cidade();
        Utilizador u = new Utilizador();
        Cidade instance = new Cidade();
        boolean result = instance.equals(cidade);
        boolean result2 = instance.equals(cidade2);
        boolean result3 = instance.equals(cidade3);
        boolean result4 = instance.equals(u);
        boolean result5 = instance.equals(cidade4);
        assertTrue(result);
        assertFalse(result2);
        assertFalse(result3);
        assertFalse(result4);
        assertFalse(result5);
    }

    /**
     * Test of getMayor method, of class Cidade.
     */
    @Test
    public void testGetMayor() {
        System.out.println("getMayor");
        Cidade instance = new Cidade();
        Utilizador expResult = new Utilizador();
        Utilizador falso = new Utilizador("a", "ola@gmail.com");
        Utilizador result = instance.getMayor();
        boolean esperado = expResult.equals(result);
        boolean esperado2 = falso.equals(result);
        assertTrue(esperado);
        assertFalse(esperado2);

    }

    /**
     * Test of setMayor method, of class Cidade.
     */
    @Test
    public void testSetMayor() {
        System.out.println("setMayor");
        Utilizador mayor = new Utilizador("a", "b@gmail.com");
        Utilizador falso = new Utilizador();
        Cidade instance = new Cidade();
        instance.setMayor(mayor);
        boolean esperado = mayor.equals(instance.getMayor());
        boolean esperado2 = falso.equals(instance.getMayor());
        assertTrue(esperado);
        assertFalse(esperado2);
    }

    /**
     * Test of atualizarMayor method, of class Cidade.
     */
    @Test
    public void testAtualizarMayor() {
        System.out.println("atualizarMayor");
        //Criar cidades
        Cidade c1 = new Cidade("c1", 1, 2, 3);
        Cidade c2 = new Cidade("c2", 11, 12, 13);
        Cidade c3 = new Cidade("c3", 21, 22, 23);

        //Criar map e adicionar ao Utilizador 1
        Map<Cidade, Integer> map = new LinkedHashMap<>();
        map.put(c1, 10);
        map.put(c2, 15);
        map.put(c3, 20);
        Utilizador u1 = new Utilizador("u1", "email1@gmail.com");
        u1.setListaCidades(map);

        //Criar map2 e adicionar ao Utilizador 2
        Utilizador u2 = new Utilizador("u2", "email2@gmail.com");
        Map<Cidade, Integer> map2 = new LinkedHashMap<>();
        u2.setListaCidades(map2);

        //Criar map3 e adicionar ao Utilizador 3
        Utilizador u3 = new Utilizador("u3", "email3@gmail.com");
        Map<Cidade, Integer> map3 = new LinkedHashMap<>();
        map3.put(c1, 1);
        map3.put(c2, 150);
        map3.put(c3, 2);
        u3.setListaCidades(map3);

        //Adicionar utilizadores ao registo
        List<Utilizador> lu = new LinkedList<>();
        lu.add(u1);
        lu.add(u2);
        lu.add(u3);
        RegistoUtilizadores ru = new RegistoUtilizadores(lu);

        Cidade instance = c2;
        c2.atualizarMayor(ru, new RegistoMayors());
        boolean esperado = instance.getMayor().equals(u3);
        assertTrue(esperado);

    }

    /**
     * Test of registarUtilizador method, of class Cidade.
     */
    @Test
    public void testRegistarUtilizador() {
        System.out.println("registarUtilizador");
        Utilizador u = new Utilizador("nick0", "1150@isep.ipp.pt");
        Utilizador u2 = new Utilizador("nick1", "11501@isep.ipp.pt");
        Cidade instance = new Cidade();
        assertTrue(instance.registarUtilizador(u));
        assertTrue(instance.registarUtilizador(u2));
        assertFalse(instance.registarUtilizador(u));
    }
    /**
     * Test of compareTo method, of class Cidade.
     */
    @Test
    public void testCompareTo() {
        System.out.println("compareTo");
        Cidade c = new Cidade();
        c.registarUtilizador(new Utilizador());
        Cidade instance = new Cidade();
        int expResult = -1;
        int result = instance.compareTo(c);
        assertEquals(expResult, result);
    }
//
    /**
     * Test of getListaRegistos method, of class Cidade.
     */
    @Test
    public void testGetListaRegistos() {
        System.out.println("getListaRegistos");
        Cidade instance = new Cidade();
        Set<Utilizador> expResult = new LinkedHashSet<>();
        Set<Utilizador> result = instance.getListaRegistos();
        assertEquals(expResult, result);
    }
//
    /**
     * Test of setListaRegistos method, of class Cidade.
     */
    @Test
    public void testSetListaRegistos() {
        System.out.println("setListaRegistos");
        Set<Utilizador> listaRegistos = new LinkedHashSet<>();
        listaRegistos.add(new Utilizador());
        Cidade instance = new Cidade();
        instance.setListaRegistos(listaRegistos);
        assertArrayEquals(instance.getListaRegistos().toArray(), listaRegistos.toArray());
    }
}
