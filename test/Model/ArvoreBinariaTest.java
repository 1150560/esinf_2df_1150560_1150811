/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.LinkedList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author NOVO
 */
public class ArvoreBinariaTest {

    public ArvoreBinariaTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getArvoreUtilizador method, of class ArvoreBinaria.
     */
    @Test
    public void testGetArvoreUtilizador() {
        System.out.println("getArvoreUtilizador");
        ArvoreBinaria instance = new ArvoreBinaria();
        BST<Mayor> expResult = new BST<>();
        BST<Mayor> result = instance.getArvoreUtilizador();
        assertEquals(expResult.toString(), result.toString());
    }

    /**
     * Test of getArvoreCidades method, of class ArvoreBinaria.
     */
    @Test
    public void testGetArvoreCidades() {
        System.out.println("getArvoreCidades");
        ArvoreBinaria instance = new ArvoreBinaria();
        BST<Cidade> expResult = new BST<>();
        BST<Cidade> result = instance.getArvoreCidades();
        assertEquals(expResult.toString(), result.toString());
    }

    /**
     * Test of setArvoreUtilizador method, of class ArvoreBinaria.
     */
    @Test
    public void testSetArvoreUtilizador() {
        System.out.println("setArvoreUtilizador");
        BST<Mayor> arvoreUtilizador = new BST<>();
        ArvoreBinaria instance = new ArvoreBinaria();
        instance.setArvoreUtilizador(arvoreUtilizador);
        assertEquals(arvoreUtilizador.toString(), instance.getArvoreUtilizador().toString());
    }

    /**
     * Test of setArvoreCidades method, of class ArvoreBinaria.
     */
    @Test
    public void testSetArvoreCidades() {
        System.out.println("setArvoreCidades");
        BST<Cidade> arvoreCidades = new BST<>();
        ArvoreBinaria instance = new ArvoreBinaria();
        instance.setArvoreCidades(arvoreCidades);
        assertEquals(arvoreCidades.toString(), instance.getArvoreCidades().toString());
    }

    /**
     * Test of criarArvoreUtilizadores method, of class ArvoreBinaria.
     */
    @Test
    public void testCriarArvoreUtilizadores() {
        System.out.println("criarArvoreUtilizadores");
        CentroDados cd = new CentroDados();
        //Ler cidades
        LerFicheiro ler = new LerFicheiro(cd.getRegistoCidades(), "cities10");
        ler.carregarCidades();

        //Ler os utilizadores
        LerFicheiro ler3 = new LerFicheiro(cd.getRegistoUtilizadores(), cd.getRegistoCidades(), "users10");
        ler3.carregarUtilizadores();
        for (Cidade c : cd.getRegistoCidades().getListaCidades()) {
            c.atualizarMayor(cd.getRegistoUtilizadores(), cd.getRegistoMayors());
        }

        ArvoreBinaria instance = new ArvoreBinaria();
        List<Mayor> expResult = new LinkedList<>();

        expResult.add(cd.getRegistoMayors().getListaMayors().get(2));
        expResult.add(cd.getRegistoMayors().getListaMayors().get(6));
        expResult.add(cd.getRegistoMayors().getListaMayors().get(5));
        expResult.add(cd.getRegistoMayors().getListaMayors().get(1));
        expResult.add(cd.getRegistoMayors().getListaMayors().get(9));
        expResult.add(cd.getRegistoMayors().getListaMayors().get(4));
        expResult.add(cd.getRegistoMayors().getListaMayors().get(3));
        expResult.add(cd.getRegistoMayors().getListaMayors().get(0));
        expResult.add(cd.getRegistoMayors().getListaMayors().get(7));
        expResult.add(cd.getRegistoMayors().getListaMayors().get(8));

        List<Mayor> result = instance.criarArvoreUtilizadores(cd.getRegistoMayors().getListaMayors());
        assertArrayEquals(expResult.toArray(), result.toArray());

    }

    /**
     * Test of criarArvoreCidades method, of class ArvoreBinaria.
     */
    @Test
    public void testCriarArvoreCidades() {
        System.out.println("criarArvoreCidades");
        CentroDados cd = new CentroDados();
        //Ler cidades
        LerFicheiro ler = new LerFicheiro(cd.getRegistoCidades(), "cities10");
        ler.carregarCidades();

        //Ler os utilizadores
        LerFicheiro ler3 = new LerFicheiro(cd.getRegistoUtilizadores(), cd.getRegistoCidades(), "users10");
        ler3.carregarUtilizadores();
        List<Cidade> lc = cd.getRegistoCidades().getListaCidades();
        ArvoreBinaria instance = new ArvoreBinaria();
        List<Cidade> expResult = new LinkedList<>();
        expResult.add(cd.getRegistoCidades().getCidade("city0"));
        expResult.add(cd.getRegistoCidades().getCidade("city1"));
        expResult.add(cd.getRegistoCidades().getCidade("city2"));
        expResult.add(cd.getRegistoCidades().getCidade("city3"));
        expResult.add(cd.getRegistoCidades().getCidade("city5"));
        expResult.add(cd.getRegistoCidades().getCidade("city6"));
        expResult.add(cd.getRegistoCidades().getCidade("city7"));
        expResult.add(cd.getRegistoCidades().getCidade("city4"));
        expResult.add(cd.getRegistoCidades().getCidade("city8"));
        expResult.add(cd.getRegistoCidades().getCidade("city9"));

        List<Cidade> result = instance.criarArvoreCidades(lc);
        assertArrayEquals(expResult.toArray(), result.toArray());
    }
}
