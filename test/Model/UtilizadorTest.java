/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author NOVO
 */
public class UtilizadorTest {

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test of getNickname method, of class Utilizador.
     */
    @Test
    public void testGetNickname() {
        System.out.println("getNickname");
        Utilizador u = new Utilizador("nick1", "email1@gmail.com");
        Utilizador instance = new Utilizador(u);
        String expResult = "nick1";
        String result = instance.getNickname();
        assertEquals(expResult, result);

    }

    /**
     * Test of getEmail method, of class Utilizador.
     */
    @Test
    public void testGetEmail() {
        System.out.println("getEmail");
        Utilizador instance = new Utilizador("nick", "email@gmail.com");
        String expResult = "email@gmail.com";
        String result = instance.getEmail();
        assertEquals(expResult, result);
    }

    /**
     * Test of getListaCidades method, of class Utilizador.
     */
    @Test
    public void testGetListaCidades() {
        System.out.println("getListaCidades");
        LinkedHashMap expResult = new LinkedHashMap();
        RegistoUtilizadores ru = new RegistoUtilizadores();
        Utilizador instance = new Utilizador("nick", "email@gmail.com", expResult, ru);
        LinkedHashMap result = (LinkedHashMap) instance.getListaCidades();
        assertEquals(expResult, result);
    }

    /**
     * Test of getListaCheckIn method, of class Utilizador.
     */
    @Test
    public void testGetListaCheckIn() {
        System.out.println("getListaCheckIn");
        Utilizador instance = new Utilizador();
        LinkedList expResult = new LinkedList<>();
        LinkedList result = instance.getListaCheckIn();
        assertEquals(expResult, result);
    }

    /**
     * Test of getPontosTotais method, of class Utilizador.
     */
    @Test
    public void testGetPontosTotais() {
        System.out.println("getPontosTotais");
        Utilizador instance = new Utilizador();
        int expResult = 0;
        int result = instance.getPontosTotais();
        assertEquals(expResult, result);
    }

    /**
     * Test of getCidadeAtual method, of class Utilizador.
     */
    @Test
    public void testGetCidadeAtual() {
        System.out.println("getCidadeAtual");
        Utilizador instance = new Utilizador();
        Cidade expResult = null;
        Cidade result = instance.getCidadeAtual();
        assertEquals(expResult, result);
    }

    /**
     * Test of getListaAmigos method, of class Utilizador.
     */
    @Test
    public void testGetListaAmigos() {
        System.out.println("getListaAmigos");
        List<Utilizador> lu = new LinkedList<>();
        LinkedHashMap rc = new LinkedHashMap();
        RegistoUtilizadores expResult = new RegistoUtilizadores(lu);
        Utilizador instance = new Utilizador("nick", "email@gmail.com", rc, expResult);
        RegistoUtilizadores result = instance.getListaAmigos();
        assertArrayEquals(expResult.getListaUtilizadores().toArray(), result.getListaUtilizadores().toArray());
    }

    /**
     * Test of setNickname method, of class Utilizador.
     */
    @Test
    public void testSetNickname() {
        System.out.println("setNickname");
        String nickname = "Este";
        Utilizador instance = new Utilizador();
        instance.setNickname(nickname);
        assertEquals(nickname, instance.getNickname());
    }

    /**
     * Test of setNickname method, of class Utilizador.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testSetNickname2() {
        System.out.println("setNickname");
        String nickname = "";
        Utilizador instance = new Utilizador();
        instance.setNickname(nickname);
        assertEquals(nickname, instance.getNickname());
    }

    /**
     * Test of setEmail method, of class Utilizador.
     */
    @Test
    public void testSetEmail() {
        System.out.println("setEmail");
        String email = "novo@gmail.com";
        Utilizador instance = new Utilizador();
        instance.setEmail(email);
        assertEquals(email, instance.getEmail());
    }

    /**
     * Test of setEmail method, of class Utilizador.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testSetEmail2() {
        System.out.println("setEmail");
        String email = "novo";
        Utilizador instance = new Utilizador();
        instance.setEmail(email);
        assertEquals(email, instance.getEmail());
    }

    /**
     * Test of setListaCidades method, of class Utilizador.
     */
    @Test
    public void testSetListaCidades() {
        System.out.println("setListaCidades");
        Cidade c = new Cidade("cidade1", 10, 10.5, 80.4);
        LinkedHashMap listaCidades = new LinkedHashMap<>();
        listaCidades.put(c, 20);
        Utilizador instance = new Utilizador();
        instance.setListaCidades(listaCidades);
        assertEquals(listaCidades, instance.getListaCidades());
    }

    /**
     * Test of setListaCheckIn method, of class Utilizador.
     */
    @Test
    public void testSetListaCheckIn() {
        System.out.println("setListaCheckIn");
        LinkedList listaCheckIn = new LinkedList<>();
        listaCheckIn.add(new Cidade("c1", 1, 2, 3));
        Utilizador instance = new Utilizador();
        instance.setListaCheckIn(listaCheckIn);
        assertArrayEquals(listaCheckIn.toArray(), instance.getListaCheckIn().toArray());
    }

    /**
     * Test of setPontosTotais method, of class Utilizador.
     */
    @Test
    public void testSetPontosTotais() {
        System.out.println("setPontosTotais");
        int pontosTotais = 20;
        Utilizador instance = new Utilizador();
        instance.setPontosTotais(pontosTotais);
        assertEquals(pontosTotais, instance.getPontosTotais());
    }

    /**
     * Test of setCidadeAtual method, of class Utilizador.
     */
    @Test
    public void testSetCidadeAtual() {
        System.out.println("setCidadeAtual");
        Cidade cidadeAtual = new Cidade("c1", 1, 2, 3);
        Utilizador instance = new Utilizador();
        instance.setCidadeAtual(cidadeAtual);
        assertEquals(cidadeAtual, instance.getCidadeAtual());
    }

    /**
     * Test of setListaAmigos method, of class Utilizador.
     */
    @Test
    public void testSetListaAmigos() {
        System.out.println("setListaAmigos");
        List<Utilizador> lu = new LinkedList<>();
        RegistoUtilizadores listaAmigos = new RegistoUtilizadores(lu);
        Utilizador instance = new Utilizador();
        instance.setListaAmigos(listaAmigos);
        assertArrayEquals(listaAmigos.getListaUtilizadores().toArray(), instance.getListaAmigos().getListaUtilizadores().toArray());
    }

    /**
     * Test of toString method, of class Utilizador.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Utilizador instance = new Utilizador();
        String expResult = "[Sem nick]";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of validaEmail method, of class Utilizador.
     */
    @Test
    public void testValidaEmail() {
        System.out.println("validaEmail");
        String email = "novo@gmail.com";
        String email2 = "novo";
        Utilizador instance = new Utilizador();
        boolean result = instance.validaEmail(email);
        boolean result2 = instance.validaEmail(email2);
        assertTrue(result);
        assertFalse(result2);
    }

    /**
     * Test of equals method, of class Utilizador.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Utilizador u = new Utilizador("nome", "email@gmail.com");
        Utilizador instance = u;
        boolean result = instance.equals(u);
        Utilizador u2 = null;
        boolean result2 = instance.equals(u2);
        Utilizador u3 = new Utilizador("nome", "email@gmail.com");
        boolean result3 = instance.equals(u3);
        Utilizador u4 = new Utilizador("falso", "email@gmail.com");
        boolean result4 = instance.equals(u4);
        assertTrue(result);
        assertFalse(result2);
        assertTrue(result3);
        assertFalse(result4);
    }

    /**
     * Test of getPontosCidade method, of class Utilizador.
     */
    @Test
    public void testGetPontosCidade() {
        System.out.println("getPontosCidade");
        String nomeCidade = "c2";
        Utilizador instance = new Utilizador();
        Cidade c1 = new Cidade("c1", 1, 2, 3);
        Cidade c2 = new Cidade("c2", 11, 12, 13);
        Cidade c3 = new Cidade("c3", 21, 22, 23);
        Map<Cidade, Integer> map = new LinkedHashMap<>();
        map.put(c1, 10);
        map.put(c2, 15);
        map.put(c3, 20);
        instance.setListaCidades(map);
        int expResult = 15;
        int result = instance.getPontosCidade(nomeCidade);
        assertEquals(expResult, result);

    }

    /**
     * Test of addPontosCidade method, of class Utilizador.
     */
    @Test
    public void testAddPontosCidade() {
        System.out.println("addPontosCidade");
        Cidade c1 = new Cidade("c1", 1, 2, 3);
        Cidade c2 = new Cidade("c2", 11, 12, 13);
        Cidade c3 = new Cidade("c3", 21, 22, 23);
        Map<Cidade, Integer> map = new LinkedHashMap<>();
        map.put(c1, 10);
        map.put(c2, 15);
        Utilizador u1 = new Utilizador("Quim", "1150811@isep.ipp.pt");
        u1.setListaCidades(map);
        int nPontos = 50;
        u1.addPontosCidade(c1, nPontos);
        int esperado = 60;
        assertEquals(u1.getPontosCidade(c1.getNome()), esperado, 0.0);

    }

    /**
     * Test of removerAmigo method, of class Utilizador.
     */
    @Test
    public void testRemoverAmigo() {
        System.out.println("removerAmigo");
        Utilizador u = new Utilizador("quim", "1150811@isep.ipp.pt");
        Utilizador u2 = new Utilizador("Diogo", "1150560@isep.ipp.pt");
        Utilizador u3 = new Utilizador("professor", "emailProf@isep.ipp.pt");

        RegistoUtilizadores listaAmigos = new RegistoUtilizadores();
        listaAmigos.addUtilizador(u2);
        listaAmigos.addUtilizador(u);
        //Adiciona a lista de amigos ao utilizador2 com u e u3
        u3.setListaAmigos(listaAmigos);
        boolean result = u3.removerAmigo(u);
        assertTrue(result);
        boolean result2 = u3.removerAmigo(u);
        assertFalse(result2);
    }

}
