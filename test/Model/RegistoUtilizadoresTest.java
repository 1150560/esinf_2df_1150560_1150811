/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Graph.Edge;
import Graph.Graph;
import Graph.Vertex;
import Matriz.AdjacencyMatrixGraph;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author NOVO
 */
public class RegistoUtilizadoresTest {

    public RegistoUtilizadoresTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getListaUtilizadores method, of class RegistoUtilizadores.
     */
    @Test
    public void testGetListaUtilizadores() {
        System.out.println("getListaUtilizadores");
        RegistoUtilizadores copia = new RegistoUtilizadores();
        RegistoUtilizadores instance = new RegistoUtilizadores(copia);
        List<Utilizador> expResult = new LinkedList<>();
        List<Utilizador> result = instance.getListaUtilizadores();
        assertArrayEquals(expResult.toArray(), result.toArray());
    }

    /**
     * Test of setListaUtilizadores method, of class RegistoUtilizadores.
     */
    @Test
    public void testSetListaUtilizadores() {
        System.out.println("setListaUtilizadores");
        List<Utilizador> lu = new LinkedList<>();
        List<Utilizador> listaUtilizadores = new LinkedList<>();
        listaUtilizadores.add(new Utilizador("nome", "email@gmail.com"));
        RegistoUtilizadores instance = new RegistoUtilizadores(lu);
        instance.setListaUtilizadores(listaUtilizadores);
        assertArrayEquals(listaUtilizadores.toArray(), instance.getListaUtilizadores().toArray());
    }

    /**
     * Test of toString method, of class RegistoUtilizadores.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        List<Utilizador> listaUtilizadores = new LinkedList<>();
        listaUtilizadores.add(new Utilizador("nome", "email@gmail.com"));
        RegistoUtilizadores instance = new RegistoUtilizadores(listaUtilizadores);
        String expResult = "A lista de utilizadores é:\n[nome]\n";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of listaUtilizadoresToString method, of class RegistoUtilizadores.
     */
    @Test
    public void testListaUtilizadoresToString() {
        System.out.println("listaUtilizadoresToString");
        List<Utilizador> listaUtilizadores = new LinkedList<>();
        listaUtilizadores.add(new Utilizador("nome", "email@gmail.com"));
        RegistoUtilizadores instance = new RegistoUtilizadores(listaUtilizadores);
        String expResult = "[nome]\n";
        String result = instance.listaUtilizadoresToString();
        assertEquals(expResult, result);
    }

    /**
     * Test of novoUtilizador method, of class RegistoUtilizadores.
     */
    @Test
    public void testNovoUtilizador() {
        System.out.println("novoUtilizador");
        RegistoUtilizadores instance = new RegistoUtilizadores();
        Utilizador expResult = new Utilizador();
        Utilizador result = instance.novoUtilizador();
        boolean esperado = expResult.equals(result);
        assertTrue(esperado);
    }

    /**
     * Test of registaUtilizador method, of class RegistoUtilizadores.
     */
    @Test
    public void testRegistaUtilizador() {
        System.out.println("registaUtilizador");
        Utilizador u = new Utilizador();
        Utilizador u2 = new Utilizador("nome", "email@gmail.com");
        List<Utilizador> lu = new LinkedList<>();
        lu.add(u);
        RegistoUtilizadores instance = new RegistoUtilizadores(lu);
        boolean result = instance.registaUtilizador(u);
        boolean result2 = instance.registaUtilizador(u2);
        assertFalse(result);
        assertTrue(result2);
    }

    /**
     * Test of valida method, of class RegistoUtilizadores.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        Utilizador u = new Utilizador();
        Utilizador u2 = new Utilizador("nome", "email@gmail.com");
        List<Utilizador> lu = new LinkedList<>();
        lu.add(u);
        RegistoUtilizadores instance = new RegistoUtilizadores(lu);
        boolean result = instance.valida(u);
        boolean result2 = instance.valida(u2);
        assertFalse(result);
        assertTrue(result2);
    }

    /**
     * Test of addUtilizador method, of class RegistoUtilizadores.
     */
    @Test
    public void testAddUtilizador() {
        System.out.println("addUtilizador");
        Utilizador u = new Utilizador("este", "outro@gmail.com");
        List<Utilizador> lu = new LinkedList<>();
        lu.add(u);
        RegistoUtilizadores ru = new RegistoUtilizadores(lu);
        RegistoUtilizadores instance = new RegistoUtilizadores();
        instance.addUtilizador(u);
        assertArrayEquals(ru.getListaUtilizadores().toArray(), instance.getListaUtilizadores().toArray());

    }

    /**
     * Test of getUtilizador method, of class RegistoUtilizadores.
     */
    @Test
    public void testGetUtilizador() {
        System.out.println("getUtilizador");
        String nickname = "User1";
        String nickname2 = "User2";
        List<Utilizador> lu = new LinkedList<>();
        lu.add(new Utilizador(nickname, "email@gmail.com"));
        RegistoUtilizadores instance = new RegistoUtilizadores(lu);
        boolean expResult = new Utilizador(nickname, "email@gmail.com").equals(instance.getUtilizador(nickname));
        boolean expResult2 = null == instance.getUtilizador(nickname2);
        assertTrue(expResult);
        assertTrue(expResult2);

    }

    /**
     * Test of isUtilizador method, of class RegistoUtilizadores.
     */
    @Test
    public void testIsUtilizador() {
        System.out.println("isUtilizador");
        String nickname = "User1";
        String nickname2 = "User2";
        List<Utilizador> lu = new LinkedList<>();
        lu.add(new Utilizador(nickname, "email@gmail.com"));
        RegistoUtilizadores instance = new RegistoUtilizadores(lu);
        boolean result = instance.isUtilizador(nickname);
        boolean result2 = instance.isUtilizador(nickname2);
        assertTrue(result);
        assertFalse(result2);

    }

    /**
     * Test of removerUtilizador method, of class RegistoUtilizadores.
     */
    @Test
    public void testRemoverUtilizador() {
        System.out.println("removerUtilizador");

        Utilizador u = new Utilizador("quim", "1150811@isep.ipp.pt");
        Utilizador u2 = new Utilizador("Diogo", "1150560@isep.ipp.pt");
        Utilizador u3 = new Utilizador("professor", "emailProf@isep.ipp.pt");

        RegistoUtilizadores instance = new RegistoUtilizadores();
        instance.addUtilizador(u2);
        instance.addUtilizador(u);
        boolean result = instance.removerUtilizador(u);
        assertTrue(result);
        boolean result2 = instance.removerUtilizador(u3);
        assertFalse(result2);
    }

    /**
     * Test of getListaAmigosLocalizacao method, of class RegistoUtilizadores.
     */
    @Test
    public void testGetListaAmigosLocalizacao() {
        System.out.println("getListaAmigosLocalizacao");

        //Criar 2 cidades
        Cidade c = new Cidade("cidade1", 10, 20, 30);
        Cidade c2 = new Cidade();

        //Criar 5 utilizadores
        Utilizador u1 = new Utilizador("u1", "email1@gmail.com");
        Utilizador u2 = new Utilizador("u2", "email2@gmail.com");
        Utilizador u3 = new Utilizador("u3", "email3@gmail.com");
        Utilizador u4 = new Utilizador("u4", "email4@gmail.com");
        Utilizador u5 = new Utilizador("u5", "email5@gmail.com");

        //Alterar a cidade atual dos utilizadores para a correta
        u1.setCidadeAtual(c);
        u3.setCidadeAtual(c);
        u4.setCidadeAtual(c);

        //Alterar a cidade atual dos utilizadores para a errada
        u2.setCidadeAtual(c2);
        u5.setCidadeAtual(c2);

        //Adicionar os utilizadores ao registo
        RegistoUtilizadores instance = new RegistoUtilizadores();
        instance.addUtilizador(u1);
        instance.addUtilizador(u2);
        instance.addUtilizador(u3);
        instance.addUtilizador(u4);
        instance.addUtilizador(u5);

        LinkedList<Utilizador> expResult = new LinkedList<>();

        //Adicionar os utilizadores corretos ao resultado esperado
        expResult.add(u1);
        expResult.add(u3);
        expResult.add(u4);

        LinkedList<Utilizador> result = instance.getListaAmigosLocalizacao(c);
        assertArrayEquals(expResult.toArray(), result.toArray());
    }

    /**
     * Test of getListaUtilizadoresInfluentes method, of class RegistoUtilizadores.
     */
    @Test
    public void testGetListaUtilizadoresInfluentes() {
        System.out.println("getListaUtilizadoresInfluentes");
        RegistoUtilizadores instance = new RegistoUtilizadores();

        //Criar utilizador 1 com 6 amigos
        Utilizador u1 = new Utilizador("quim", "email@isep.ipp.pt");
        u1.getListaAmigos().addUtilizador(new Utilizador());
        u1.getListaAmigos().addUtilizador(new Utilizador());
        u1.getListaAmigos().addUtilizador(new Utilizador());
        u1.getListaAmigos().addUtilizador(new Utilizador());
        u1.getListaAmigos().addUtilizador(new Utilizador());
        u1.getListaAmigos().addUtilizador(new Utilizador());

        //criar utilizador u2 com 3 amigos
        Utilizador u2 = new Utilizador("diog", "email@isep.ipp.pt");
        u2.getListaAmigos().addUtilizador(new Utilizador());
        u2.getListaAmigos().addUtilizador(new Utilizador());
        u2.getListaAmigos().addUtilizador(new Utilizador());

        //Criar utilizador3 com 4 amigos
        Utilizador u3 = new Utilizador("diog02", "email@isep.ipp.pt");
        u3.getListaAmigos().addUtilizador(new Utilizador());
        u3.getListaAmigos().addUtilizador(new Utilizador());
        u3.getListaAmigos().addUtilizador(new Utilizador());
        u3.getListaAmigos().addUtilizador(new Utilizador());

        //Criar utilizador u4 com 3 amigos
        Utilizador u4 = new Utilizador("diog03", "email@isep.ipp.pt");
        u4.getListaAmigos().addUtilizador(new Utilizador());
        u4.getListaAmigos().addUtilizador(new Utilizador());
        u4.getListaAmigos().addUtilizador(new Utilizador());

        //Adicionar utilizadores ao registo
        instance.addUtilizador(u4);
        instance.addUtilizador(u3);
        instance.addUtilizador(u1);
        instance.addUtilizador(u2);

        Map<Utilizador, Integer> expResult = new LinkedHashMap<>();

        //Certo
        expResult.put(u1, 6);
        expResult.put(u3, 4);
        expResult.put(u4, 3);
        expResult.put(u2, 3);

        //Errado 
//        expResult.put(u2, 3);
//        expResult.put(u3, 4);
//        expResult.put(u4, 3);
//        expResult.put(u1, 6);
        Map<Utilizador, Integer> result = instance.getListaUtilizadoresInfluentes();
        if (!result.keySet().toString().equals(expResult.keySet().toString())) {
            assertTrue(false);
        }
        assertEquals(expResult, result);
    }
}
