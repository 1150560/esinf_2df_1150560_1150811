/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Graph.Graph;
import Graph.Vertex;
import Matriz.AdjacencyMatrixGraph;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author w8
 */
public class MatrizAdjacenciasTest {

    CentroDados cd;

    MatrizAdjacencias instance;

    public MatrizAdjacenciasTest() {
        this.cd = new CentroDados();
        this.instance = new MatrizAdjacencias();
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {

        //Ler cidades
        LerFicheiro ler = new LerFicheiro(cd.getRegistoCidades(), "cities10");
        ler.carregarCidades();

        //Ler os utilizadores
        LerFicheiro ler3 = new LerFicheiro(cd.getRegistoUtilizadores(), cd.getRegistoCidades(), "users10");
        ler3.carregarUtilizadores();
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getMatrizAdjacencias method, of class MatrizAdjacencias.
     */
    @Test
    public void testGetMatrizAdjacencias() {
        System.out.println("getMatrizAdjacencias");
        AdjacencyMatrixGraph expResult = new AdjacencyMatrixGraph();
        AdjacencyMatrixGraph result = instance.getMatrizAdjacencias();
        assertEquals(expResult, result);
    }

    /**
     * Test of setMatrizAdjacencias method, of class MatrizAdjacencias.
     */
    @Test
    public void testSetMatrizAdjacencias() {
        System.out.println("setMatrizAdjacencias");
        AdjacencyMatrixGraph result = new AdjacencyMatrixGraph();
        instance.setMatrizAdjacencias(result);
        assertEquals(result, instance.getMatrizAdjacencias());

    }

    /**
     * Test of carregarConexoes method, of class LerFicheiro.
     */
    @Test
    public void testCarregarConexoes() {
        System.out.println("carregarConexoes");

        RegistoCidades rc = cd.getRegistoCidades();
        String nomeFicheiroConexoes = "cityConnections10";

        //Criar matrix esperada
        AdjacencyMatrixGraph<Cidade, Double> esperado = new AdjacencyMatrixGraph<>();

        //Criar vertex
        esperado.insertVertex(rc.getCidade("city0"));
        esperado.insertVertex(rc.getCidade("city3"));
        esperado.insertVertex(rc.getCidade("city4"));
        esperado.insertVertex(rc.getCidade("city1"));
        esperado.insertVertex(rc.getCidade("city9"));
        esperado.insertVertex(rc.getCidade("city7"));
        esperado.insertVertex(rc.getCidade("city2"));
        esperado.insertVertex(rc.getCidade("city6"));
        esperado.insertVertex(rc.getCidade("city8"));
        esperado.insertVertex(rc.getCidade("city5"));

        //Criar edges
        esperado.insertEdge(rc.getCidade("city0"), rc.getCidade("city3"), 38.0);
        esperado.insertEdge(rc.getCidade("city0"), rc.getCidade("city4"), 75.0);
        esperado.insertEdge(rc.getCidade("city1"), rc.getCidade("city9"), 68.0);
        esperado.insertEdge(rc.getCidade("city1"), rc.getCidade("city7"), 41.0);
        esperado.insertEdge(rc.getCidade("city2"), rc.getCidade("city3"), 51.0);
        esperado.insertEdge(rc.getCidade("city2"), rc.getCidade("city6"), 55.0);
        esperado.insertEdge(rc.getCidade("city2"), rc.getCidade("city7"), 59.0);
        esperado.insertEdge(rc.getCidade("city3"), rc.getCidade("city2"), 59.0);
        esperado.insertEdge(rc.getCidade("city3"), rc.getCidade("city6"), 29.0);
        esperado.insertEdge(rc.getCidade("city4"), rc.getCidade("city7"), 29.0);
        esperado.insertEdge(rc.getCidade("city4"), rc.getCidade("city0"), 36.0);
        esperado.insertEdge(rc.getCidade("city4"), rc.getCidade("city9"), 24.0);
        esperado.insertEdge(rc.getCidade("city4"), rc.getCidade("city8"), 34.0);
        esperado.insertEdge(rc.getCidade("city5"), rc.getCidade("city7"), 16.0);
        esperado.insertEdge(rc.getCidade("city5"), rc.getCidade("city9"), 57.0);
        esperado.insertEdge(rc.getCidade("city5"), rc.getCidade("city0"), 73.0);
        esperado.insertEdge(rc.getCidade("city6"), rc.getCidade("city3"), 29.0);
        esperado.insertEdge(rc.getCidade("city6"), rc.getCidade("city1"), 69.0);
        esperado.insertEdge(rc.getCidade("city7"), rc.getCidade("city9"), 37.0);
        esperado.insertEdge(rc.getCidade("city7"), rc.getCidade("city8"), 90.0);
        esperado.insertEdge(rc.getCidade("city7"), rc.getCidade("city0"), 41.0);
        esperado.insertEdge(rc.getCidade("city7"), rc.getCidade("city6"), 29.0);
        esperado.insertEdge(rc.getCidade("city8"), rc.getCidade("city4"), 114.0);
        esperado.insertEdge(rc.getCidade("city8"), rc.getCidade("city7"), 52.0);
        esperado.insertEdge(rc.getCidade("city8"), rc.getCidade("city5"), 79.0);
        esperado.insertEdge(rc.getCidade("city8"), rc.getCidade("city1"), 89.0);
        esperado.insertEdge(rc.getCidade("city8"), rc.getCidade("city9"), 48.0);
        esperado.insertEdge(rc.getCidade("city9"), rc.getCidade("city6"), 76.0);
        esperado.insertEdge(rc.getCidade("city9"), rc.getCidade("city0"), 75.0);

        boolean resultado = instance.carregarConexoes(nomeFicheiroConexoes, cd.getRegistoCidades());
        boolean resultado2 = instance.carregarConexoes("ficheiro mal", cd.getRegistoCidades());
        assertTrue(resultado);
        assertFalse(resultado2);
        assertEquals(esperado, instance.getMatrizAdjacencias());
    }

    /**
     * Test of amigosProximos method, of class MatrizAdjacencias.
     */
    @Test
    public void testAmigosProximos() {
        System.out.println("amigosProximos");
        MapAdjacencias map = new MapAdjacencias();
        map.mapAdjacencias(cd.getRegistoUtilizadores().getListaUtilizadores());
        instance.carregarConexoes("cityConnections10", cd.getRegistoCidades());

        Object[] lista = map.getMap().allkeyVerts();
        Utilizador u0 = (Utilizador) lista[0];
        double distancia = 76;

        List<Utilizador> expResult = new LinkedList<>();
        expResult.add((Utilizador) lista[1]);

        List<Utilizador> result = instance.amigosProximos(map, u0, distancia);
        assertEquals(expResult, result);
    }

    /**
     * Test of caminhoMaisCurto method, of class MatrizAdjacencias.
     */
    @Test
    public void testCaminhoMaisCurto() {
        System.out.println("caminhoMaisCurto");
        MapAdjacencias map = new MapAdjacencias();
        map.mapAdjacencias(cd.getRegistoUtilizadores().getListaUtilizadores());
        instance.carregarConexoes("cityConnections10", cd.getRegistoCidades());

        Object[] lista = map.getMap().allkeyVerts();
        Utilizador u1 = (Utilizador) lista[0];
        Utilizador u2 = (Utilizador) lista[9];
        Map<LinkedList<Cidade>, Double> expResult = new LinkedHashMap<>();
        LinkedList<Cidade> listaCidades = new LinkedList<>();

        listaCidades.add(cd.getRegistoCidades().getCidade("city6"));
        listaCidades.add(cd.getRegistoCidades().getCidade("city7"));
        listaCidades.add(cd.getRegistoCidades().getCidade("city4"));
        listaCidades.add(cd.getRegistoCidades().getCidade("city8"));
        double esperado = 92.0;

        expResult.put(listaCidades, esperado);

        Map<LinkedList<Cidade>, Double> result = instance.caminhoMaisCurto(u1, u2);
        assertEquals(expResult, result);
    }

    /**
     * Test of nAmigosCidade method, of class MatrizAdjacencias.
     */
    @Test
    public void testNAmigosCidade() {
        System.out.println("nAmigosCidade");

        MapAdjacencias map = new MapAdjacencias();
        map.mapAdjacencias(cd.getRegistoUtilizadores().getListaUtilizadores());

        Object[] lista = map.getMap().allkeyVerts();
        Utilizador u0 = (Utilizador) lista[0];

        List<Utilizador> listaAmigos = map.listaAmigos(u0);
        Cidade c = cd.getRegistoCidades().getCidade("city8");
        int expResult = 2;
        int result = instance.nAmigosCidade(listaAmigos, c);
        assertEquals(expResult, result);
    }

    /**
     * Test of getListaCidadesMaisAmigos method, of class MatrizAdjacencias.
     */
    @Test
    public void testGetListaCidadesMaisAmigos() {
        System.out.println("getListaCidadesMaisAmigos");

        MapAdjacencias map = new MapAdjacencias();
        map.mapAdjacencias(cd.getRegistoUtilizadores().getListaUtilizadores());

        instance.carregarConexoes("cityConnections10", cd.getRegistoCidades());

        Object[] lista = map.getMap().allkeyVerts();
        Utilizador u6 = (Utilizador) lista[5];

        List<Utilizador> listaAmigos = map.listaAmigos(u6);
        List<Cidade> expResult = new LinkedList<>();
        expResult.add(cd.getRegistoCidades().getCidade("city4"));
        expResult.add(cd.getRegistoCidades().getCidade("city9"));
        expResult.add(cd.getRegistoCidades().getCidade("city2"));
        expResult.add(cd.getRegistoCidades().getCidade("city8"));

        List<Cidade> result = instance.getListaCidadesMaisAmigos(listaAmigos);
        assertEquals(expResult, result);
    }

    /**
     * Test of tamanhoCaminho method, of class MatrizAdjacencias.
     */
    @Test
    public void testTamanhoCaminho() {
        System.out.println("tamanhoCaminho");

        instance.carregarConexoes("cityConnections10", cd.getRegistoCidades());

        List<Cidade> lista = new LinkedList<>();
        lista.add(cd.getRegistoCidades().getCidade("city4"));
        lista.add(cd.getRegistoCidades().getCidade("city7"));
        lista.add(cd.getRegistoCidades().getCidade("city2"));

        double expResult = 88;

        double result = instance.tamanhoCaminho(lista);
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of caminhoMinObrigatorio method, of class MatrizAdjacencias.
     */
    @Test
    public void testCaminhoMinObrigatorio() {
        System.out.println("caminhoMinObrigatorio");

        MapAdjacencias map = new MapAdjacencias();
        map.mapAdjacencias(cd.getRegistoUtilizadores().getListaUtilizadores());

        instance.carregarConexoes("cityConnections10", cd.getRegistoCidades());

        Object[] lista = map.getMap().allkeyVerts();
        Utilizador u1 = (Utilizador) lista[4];
        Utilizador u2 = (Utilizador) lista[6];

        List<Cidade> expResult = new LinkedList<>();
        expResult.add(cd.getRegistoCidades().getCidade("city4"));
        expResult.add(cd.getRegistoCidades().getCidade("city9"));
        expResult.add(cd.getRegistoCidades().getCidade("city5"));
        expResult.add(cd.getRegistoCidades().getCidade("city7"));
        expResult.add(cd.getRegistoCidades().getCidade("city6"));
        expResult.add(cd.getRegistoCidades().getCidade("city3"));
        expResult.add(cd.getRegistoCidades().getCidade("city2"));

        List<Cidade> result = instance.caminhoMinObrigatorio(map, u1, u2);
        assertEquals(expResult, result);

    }
}
