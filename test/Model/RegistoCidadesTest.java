/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Matriz.AdjacencyMatrixGraph;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;

/**
 *
 * @author w8
 */
public class RegistoCidadesTest {

    public RegistoCidadesTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test of getListaCidades method, of class RegistoCidades.
     */
    @Test
    public void testGetListaCidades() {
        System.out.println("getListaCidades");
        RegistoCidades instance = new RegistoCidades();
        List<Cidade> expResult = new LinkedList<>();
        List<Cidade> result = instance.getListaCidades();
        assertArrayEquals(expResult.toArray(), result.toArray());
    }
//

    /**
     * Test of setListaCidades method, of class RegistoCidades.
     */
    @Test
    public void testSetListaCidades() {
        System.out.println("setListaCidades");
        List<Cidade> listaCidades = new LinkedList<>();
        RegistoCidades instance = new RegistoCidades();
        instance.setListaCidades(listaCidades);
        assertArrayEquals(listaCidades.toArray(), instance.getListaCidades().toArray());
    }
//

    /**
     * Test of toString method, of class RegistoCidades.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        RegistoCidades instance = new RegistoCidades();
        instance.addCidade(new Cidade());
        String expResult = "A lista de cidades é:\n[sem nome]\n";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
//

    /**
     * Test of listaCidadesToString method, of class RegistoCidades.
     */
    @Test
    public void testListaCidadesToString() {
        System.out.println("listaCidadesToString");
        RegistoCidades instance = new RegistoCidades();
        String expResult = "";
        String result = instance.listaCidadesToString();
        assertEquals(expResult, result);
    }
//

    /**
     * Test of novoCidade method, of class RegistoCidades.
     */
    @Test
    public void testNovoCidade() {
        System.out.println("novoCidade");
        RegistoCidades instance = new RegistoCidades();
        Cidade expResult = new Cidade();
        Cidade result = instance.novoCidade();
        assertTrue(expResult.equals(result));
    }
//

    /**
     * Test of registaCidade method, of class RegistoCidades.
     */
    @Test
    public void testRegistaCidade() {
        System.out.println("registaCidade");
        Cidade c = new Cidade();
        RegistoCidades instance = new RegistoCidades();
        boolean result = instance.registaCidade(c);
        assertTrue(result);
        boolean result2 = instance.registaCidade(c);
        assertFalse(result2);
    }
//

    /**
     * Test of valida method, of class RegistoCidades.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        Cidade c = new Cidade();
        RegistoCidades instance = new RegistoCidades();
        boolean result = instance.valida(c);
        assertTrue(result);
        instance.addCidade(c);
        boolean result2 = instance.valida(c);
        assertFalse(result2);
    }

    /**
     * Test of addCidade method, of class RegistoCidades.
     */
    @Test
    public void testAddCidade() {
        System.out.println("addCidade");
        Cidade c = new Cidade();
        RegistoCidades instance = new RegistoCidades();
        instance.addCidade(c);
        int expResult = 1;
        int result = instance.getListaCidades().size();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of ordenarCidades method, of class RegistoCidades.
     */
    @Test
    public void testOrdenarCidades() {
        System.out.println("ordenarCidades");

        List<Cidade> lc = new LinkedList<>();

        //Criar 3 cidades
        Cidade c1 = new Cidade("cidade1", 10, 15, 20);
        Cidade c2 = new Cidade("cidade2", 10, 15, 20);
        Cidade c3 = new Cidade("cidade3", 10, 15, 20);

        //Criar 3 utilizadores e adicionar uma cidade e pontos
        Utilizador u1 = new Utilizador("nome1", "e1@gmail.com");
        u1.getListaCidades().put(c1, 20);
        Utilizador u2 = new Utilizador("nome2", "e2@gmail.com");
        u2.getListaCidades().put(c2, 30);
        Utilizador u3 = new Utilizador("nome3", "e3@gmail.com");
        u3.getListaCidades().put(c3, 10);

        //Modificar o mayor da cidade
        c1.setMayor(u1);
        c2.setMayor(u2);
        c3.setMayor(u3);

        //Adicionar as cidades ao registo
        lc.add(c1);
        lc.add(c2);
        lc.add(c3);
        RegistoCidades instance = new RegistoCidades(lc);

        Map<Cidade, Integer> expResult = new LinkedHashMap<>();

        expResult.put(c2, 30);
        expResult.put(c1, 20);
        expResult.put(c3, 10);

        Map<Cidade, Integer> result = instance.ordenarCidades();

        if (!result.keySet().toString().equals(expResult.keySet().toString())) {
            assertTrue(false);
        }
        assertEquals(expResult, result);

    }

    /**
     * Test of nPontosCidades method, of class RegistoCidades.
     */
    @Test
    public void testGetCidade() {
        System.out.println("getCidade");
        String nomeCidade = "cidade2";
        String nomeCidade2 = "cidade3001";
        RegistoCidades rc = new RegistoCidades();
        RegistoCidades instance = new RegistoCidades(rc);
        instance.addCidade(new Cidade("cidade1", 10, 20, 30));
        instance.addCidade(new Cidade("cidade2", 20, 10, 50));
        instance.addCidade(new Cidade("cidade3", 30, 50, 40));
        Cidade expResult = new Cidade("cidade2", 20, 10, 50);
        Cidade result = instance.getCidade(nomeCidade);
        boolean esperado = expResult.equals(result);
        assertTrue(esperado);
        assertNull(instance.getCidade(nomeCidade2));

    }

}
