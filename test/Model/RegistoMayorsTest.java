/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.LinkedList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author NOVO
 */
public class RegistoMayorsTest {

    public RegistoMayorsTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getListaMayors method, of class RegistoMayors.
     */
    @Test
    public void testGetListaMayors() {
        System.out.println("getListaMayors");
        RegistoMayors instance = new RegistoMayors();
        List<Mayor> expResult = new LinkedList<>();
        List<Mayor> result = instance.getListaMayors();
        assertArrayEquals(expResult.toArray(), result.toArray());
    }
//

    /**
     * Test of setListaMayors method, of class RegistoMayors.
     */
    @Test
    public void testSetListaMayors() {
        System.out.println("setListaMayors");
        List<Mayor> listaMayors = new LinkedList<>();
        RegistoMayors instance = new RegistoMayors();
        instance.setListaMayors(listaMayors);
        assertArrayEquals(listaMayors.toArray(), instance.getListaMayors().toArray());
    }
//

    /**
     * Test of toString method, of class RegistoMayors.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        RegistoMayors instance = new RegistoMayors();
        instance.addMayor(new Mayor());
        String expResult = "A lista de mayors é:\n[0]\n";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of listaMayorsToString method, of class RegistoMayors.
     */
    @Test
    public void testListaMayorsToString() {
        System.out.println("listaMayorsToString");
        RegistoMayors instance = new RegistoMayors();
        String expResult = "";
        String result = instance.listaMayorsToString();
        assertEquals(expResult, result);
    }

    /**
     * Test of registaMayor method, of class RegistoMayors.
     */
    @Test
    public void testRegistaMayor() {
        System.out.println("registaMayor");
        Mayor c = new Mayor();
        RegistoMayors instance = new RegistoMayors();
        boolean result = instance.registaMayor(c);
        assertTrue(result);
        boolean result2 = instance.registaMayor(c);
        assertFalse(result2);
    }
//

    /**
     * Test of valida method, of class RegistoMayors.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        Mayor c = new Mayor();
        RegistoMayors instance = new RegistoMayors();
        boolean result = instance.valida(c);
        assertTrue(result);
        instance.addMayor(c);
        boolean result2 = instance.valida(c);
        assertFalse(result2);
    }

    /**
     * Test of addMayor method, of class RegistoMayors.
     */
    @Test
    public void testAddMayor() {
        System.out.println("addMayor");
        Mayor c = new Mayor();
        RegistoMayors instance = new RegistoMayors();
        instance.addMayor(c);
        int expResult = 1;
        int result = instance.getListaMayors().size();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of setMayorMayor method, of class RegistoMayors.
     */
    @Test
    public void testSetMayorCidade() {
        System.out.println("setMayorCidade");
        Mayor novo = new Mayor(new Utilizador("a2", "a@gmail.com"), new Cidade("c1", 1, 1, 1), 15);
        Mayor antigo = new Mayor(new Utilizador("a", "a@gmail.com"), new Cidade("c1", 1, 1, 1), 10);
        RegistoMayors instance = new RegistoMayors();
        instance.registaMayor(antigo);
        instance.setMayorCidade(novo);
        boolean esperado = instance.getListaMayors().get(0).getPontos() == novo.getPontos() && instance.getListaMayors().get(0).getUtilizador().equals(novo.getUtilizador());
        assertTrue(esperado);

    }
}
