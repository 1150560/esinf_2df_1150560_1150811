/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.LinkedList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author NOVO
 */
public class CentroDadosTest {

    public CentroDadosTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    /**
     * Test of getRegistoUtilizadores method, of class CentroDados.
     */
    @Test
    public void testGetRegistoUtilizadores() {
        System.out.println("getRegistoUtilizadores");
        CentroDados instance = new CentroDados();
        RegistoUtilizadores expResult = new RegistoUtilizadores();
        RegistoUtilizadores result = instance.getRegistoUtilizadores();
        assertArrayEquals(expResult.getListaUtilizadores().toArray(), result.getListaUtilizadores().toArray());
    }

    /**
     * Test of getRegistoCidades method, of class CentroDados.
     */
    @Test
    public void testGetRegistoCidades() {
        System.out.println("getRegistoCidades");
        RegistoUtilizadores ru = new RegistoUtilizadores();
        RegistoCidades rc = new RegistoCidades();
        CentroDados instance = new CentroDados(ru, rc);
        RegistoCidades expResult = rc;
        RegistoCidades result = instance.getRegistoCidades();
        assertArrayEquals(expResult.getListaCidades().toArray(), result.getListaCidades().toArray());
    }

        /**
     * Test of getAdjacencyMatrix method, of class CentroDados.
     */
    @Test
    public void testGetAdjacencyMatrix() {
        System.out.println("getAdjacencyMatrix");
        CentroDados instance = new CentroDados();
        MatrizAdjacencias expResult = new MatrizAdjacencias();
        MatrizAdjacencias result = instance.getAdjacencyMatrix();
        assertTrue(expResult.getMatrizAdjacencias().equals(result.getMatrizAdjacencias()));
    }
    
     /**
     * Test of getMapAdjacencias method, of class CentroDados.
     */
    @Test
    public void testGetMapAdjacencias() {
        System.out.println("getMapAdjacencias");
        CentroDados instance = new CentroDados();
        MapAdjacencias expResult = new MapAdjacencias();
        MapAdjacencias result = instance.getMapAdjacencias();
        assertTrue(result.getMap().equals(expResult.getMap()));
    }
    
    /**
     * Test of setRegistoUtilizadores method, of class CentroDados.
     */
    @Test
    public void testSetRegistoUtilizadores() {
        System.out.println("setRegistoUtilizadores");
        List<Utilizador> lu = new LinkedList<>();
        lu.add(new Utilizador("a", "b@gmail.com"));
        RegistoUtilizadores registoUtilizadores = new RegistoUtilizadores(lu);
        CentroDados instance = new CentroDados();
        instance.setRegistoUtilizadores(registoUtilizadores);
        assertArrayEquals(registoUtilizadores.getListaUtilizadores().toArray(), instance.getRegistoUtilizadores().getListaUtilizadores().toArray());
    }

    /**
     * Test of setRegistoCidades method, of class CentroDados.
     */
    @Test
    public void testSetRegistoCidades() {
        System.out.println("setRegistoCidades");
        List<Cidade> lc = new LinkedList<>();
        lc.add(new Cidade("a",10,12,13));
        RegistoCidades registoCidades = new RegistoCidades(lc);
        CentroDados instance = new CentroDados();
        instance.setRegistoCidades(registoCidades);
        assertArrayEquals(registoCidades.getListaCidades().toArray(), instance.getRegistoCidades().getListaCidades().toArray());
    }


    /**
     * Test of setAdjacencyMatrix method, of class CentroDados.
     */
    @Test
    public void testSetAdjacencyMatrix() {
        System.out.println("setAdjacencyMatrix");
        MatrizAdjacencias AdjacencyMatrix = new MatrizAdjacencias();
        CentroDados instance = new CentroDados();
        instance.setAdjacencyMatrix(AdjacencyMatrix);
        assertEquals(instance.getAdjacencyMatrix(), AdjacencyMatrix);
    }

    /**
     * Test of setMapAdjacencias method, of class CentroDados.
     */
    @Test
    public void testSetMapAdjacencias() {
        System.out.println("setMapAdjacencias");
        MapAdjacencias mapAdjacencias = new MapAdjacencias();
        CentroDados instance = new CentroDados();
        instance.setMapAdjacencias(mapAdjacencias);
        assertEquals(instance.getMapAdjacencias(), mapAdjacencias);
    }
}
