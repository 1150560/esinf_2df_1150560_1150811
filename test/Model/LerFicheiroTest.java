/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Matriz.AdjacencyMatrixGraph;
import java.io.FileNotFoundException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;

/**
 *
 * @author w8
 */
public class LerFicheiroTest {

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test of getRc method, of class LerFicheiro.
     */
    @Test
    public void testGetRc() {
        System.out.println("getRc");
        RegistoCidades expResult = new RegistoCidades();
        LerFicheiro instance = new LerFicheiro(expResult, "nome");
        RegistoCidades result = instance.getRc();
        assertArrayEquals(expResult.getListaCidades().toArray(), result.getListaCidades().toArray());
    }

    /**
     * Test of getRu method, of class LerFicheiro.
     */
    @Test
    public void testGetRu() {
        System.out.println("getRu");
        RegistoUtilizadores expResult = new RegistoUtilizadores();
        LerFicheiro instance = new LerFicheiro(expResult, new RegistoCidades(), "nome");
        RegistoUtilizadores result = instance.getRu();
        assertArrayEquals(expResult.getListaUtilizadores().toArray(), result.getListaUtilizadores().toArray());
    }

    /**
     * Test of getNomeFicheiro method, of class LerFicheiro.
     */
    @Test
    public void testGetNomeFicheiro() {
        System.out.println("getNomeFicheiro");
        String expResult = "esteNome";
        LerFicheiro instance = new LerFicheiro(new RegistoCidades(), expResult);
        String result = instance.getNomeFicheiro();
        assertEquals(expResult + ".txt", result);

    }

    /**
     * Test of setRc method, of class LerFicheiro.
     */
    @Test
    public void testSetRc() {
        System.out.println("setRc");
        RegistoCidades rc = new RegistoCidades();
        LerFicheiro instance = new LerFicheiro(rc, "nome");
        RegistoCidades rc2 = new RegistoCidades();
        rc2.addCidade(new Cidade("a", 1, 2, 3));
        instance.setRc(rc2);
        assertArrayEquals(rc2.getListaCidades().toArray(), instance.getRc().getListaCidades().toArray());
    }

    /**
     * Test of setRu method, of class LerFicheiro.
     */
    @Test
    public void testSetRu() {
        System.out.println("setRu");
        RegistoUtilizadores ru = new RegistoUtilizadores();
        LerFicheiro instance = new LerFicheiro(ru, new RegistoCidades(), "nome");
        RegistoUtilizadores ru2 = new RegistoUtilizadores();
        ru2.addUtilizador(new Utilizador("a", "e@gmail.com"));
        instance.setRu(ru2);
        assertArrayEquals(ru2.getListaUtilizadores().toArray(), instance.getRu().getListaUtilizadores().toArray());
    }

    /**
     * Test of setNomeFicheiro method, of class LerFicheiro.
     */
    @Test
    public void testSetNomeFicheiro() {
        System.out.println("setNomeFicheiro");
        LerFicheiro instance = new LerFicheiro(new RegistoCidades(), "semNome");
        String nomeFicheiro = "novoNome";
        instance.setNomeFicheiro(nomeFicheiro);
        assertEquals(nomeFicheiro + ".txt", instance.getNomeFicheiro());
    }

    /**
     * Test of carregarCidades method, of class LerFicheiro.
     */
    @Test
    public void testCarregarCidades() {
        System.out.println("carregarCidades");
        String nomeFicheiro = "cities10";
        RegistoCidades rc = new RegistoCidades();
        rc.addCidade(new Cidade("Porto", 1, 2, 3));

        RegistoCidades r = new RegistoCidades();
        r.addCidade(new Cidade("Porto", 1, 2, 3));
        r.addCidade(new Cidade("city0", 28, 41.243345, -8.674084));
        r.addCidade(new Cidade("city1", 72, 41.237364, -8.846746));
        r.addCidade(new Cidade("city2", 81, 40.519841, -8.085113));
        r.addCidade(new Cidade("city3", 42, 41.118700, -8.589700));
        r.addCidade(new Cidade("city4", 64, 41.467407, -8.964340));
        r.addCidade(new Cidade("city5", 74, 41.337408, -8.291943));
        r.addCidade(new Cidade("city6", 80, 41.314965, -8.423371));
        r.addCidade(new Cidade("city7", 11, 40.822244, -8.794953));
        r.addCidade(new Cidade("city8", 7, 40.781886, -8.697502));
        r.addCidade(new Cidade("city9", 65, 40.851360, -8.136585));

        LerFicheiro instance = new LerFicheiro(rc, nomeFicheiro);
        instance.carregarCidades();
        assertEquals(r.getListaCidades().toString(), rc.getListaCidades().toString());
    }

    /**
     * Test of carregarCidades method, of class LerFicheiro.
     */
    @Test
    public void testCarregarCidades2() {
        System.out.println("carregarCidades");
        String nomeFicheiroMal = "cities1";
        String nomeFicheiroMal2 = "users10";
        RegistoCidades rc = new RegistoCidades();
        RegistoCidades r = new RegistoCidades();
        r.addCidade(new Cidade());
        LerFicheiro instance = new LerFicheiro(rc, nomeFicheiroMal);

        assertFalse(instance.carregarCidades());

        instance.setNomeFicheiro(nomeFicheiroMal2);
        assertFalse(instance.carregarCidades());

    }

    /**
     * Test of carregarUtilizadores method, of class LerFicheiro.
     */
    @Test
    public void testCarregarUtilizadores() {
        System.out.println("carregarUtilizadores");

        String nomeFicheiro = "users10";
        String nomeFicheiroCidades = "cities10";
        RegistoUtilizadores ru = new RegistoUtilizadores();
        RegistoCidades rc = new RegistoCidades();

        //Criar 10 cidades
        Cidade c0 = new Cidade("city0", 28, 41.243345, -8.674084);
        Cidade c1 = new Cidade("city1", 72, 41.237364, -8.846746);
        Cidade c2 = new Cidade("city2", 81, 40.519841, -8.085113);
        Cidade c3 = new Cidade("city3", 42, 41.118700, -8.589700);
        Cidade c4 = new Cidade("city4", 64, 41.467407, -8.964340);
        Cidade c5 = new Cidade("city5", 74, 41.337408, -8.291943);
        Cidade c6 = new Cidade("city6", 80, 41.314965, -8.423371);
        Cidade c7 = new Cidade("city7", 11, 40.822244, -8.794953);
        Cidade c8 = new Cidade("city8", 7, 40.781886, -8.697502);
        Cidade c9 = new Cidade("city9", 65, 40.851360, -8.136585);

        //Adicionar as 10 cidades ao registo de cidades
        rc.addCidade(c0);
        rc.addCidade(c1);
        rc.addCidade(c2);
        rc.addCidade(c3);
        rc.addCidade(c4);
        rc.addCidade(c5);
        rc.addCidade(c6);
        rc.addCidade(c7);
        rc.addCidade(c8);
        rc.addCidade(c9);

        RegistoUtilizadores r = new RegistoUtilizadores();

        //Criar 10 utilizadores
        Utilizador u0 = new Utilizador("nick0", "mail_0_@sapo.pt");
        Utilizador u1 = new Utilizador("nick1", "mail_1_@sapo.pt");
        Utilizador u2 = new Utilizador("nick2", "mail_2_@sapo.pt");
        Utilizador u3 = new Utilizador("nick3", "mail_3_@sapo.pt");
        Utilizador u4 = new Utilizador("nick4", "mail_4_@sapo.pt");
        Utilizador u5 = new Utilizador("nick5", "mail_5_@sapo.pt");
        Utilizador u6 = new Utilizador("nick6", "mail_6_@sapo.pt");
        Utilizador u7 = new Utilizador("nick7", "mail_7_@sapo.pt");
        Utilizador u8 = new Utilizador("nick8", "mail_8_@sapo.pt");
        Utilizador u9 = new Utilizador("nick9", "mail_9_@sapo.pt");

        //Adicionar cidades aos utilizadores
        u0.getListaCheckIn().add(c4);
        u0.addPontosCidade(c4, c4.getPontos());
        u0.getListaCheckIn().add(c6);
        u0.addPontosCidade(c6, c6.getPontos());
        u1.getListaCheckIn().add(c5);
        u1.addPontosCidade(c5, c5.getPontos());
        u1.getListaCheckIn().add(c9);
        u1.addPontosCidade(c9, c9.getPontos());
        u1.getListaCheckIn().add(c4);
        u1.addPontosCidade(c4, c4.getPontos());
        u2.getListaCheckIn().add(c0);
        u2.addPontosCidade(c0, c0.getPontos());
        u2.getListaCheckIn().add(c2);
        u2.addPontosCidade(c2, c2.getPontos());
        u3.getListaCheckIn().add(c8);
        u3.addPontosCidade(c8, c8.getPontos());
        u4.getListaCheckIn().add(c6);
        u4.addPontosCidade(c6, c6.getPontos());
        u4.getListaCheckIn().add(c8);
        u4.addPontosCidade(c8, c8.getPontos());
        u5.getListaCheckIn().add(c7);
        u5.addPontosCidade(c7, c7.getPontos());
        u5.getListaCheckIn().add(c9);
        u5.addPontosCidade(c9, c9.getPontos());
        u6.getListaCheckIn().add(c1);
        u6.addPontosCidade(c1, c1.getPontos());
        u6.getListaCheckIn().add(c5);
        u6.addPontosCidade(c5, c5.getPontos());
        u7.getListaCheckIn().add(c9);
        u7.addPontosCidade(c9, c9.getPontos());
        u8.getListaCheckIn().add(c4);
        u8.addPontosCidade(c4, c4.getPontos());
        u8.getListaCheckIn().add(c7);
        u8.addPontosCidade(c7, c7.getPontos());
        u8.getListaCheckIn().add(c3);
        u8.addPontosCidade(c3, c3.getPontos());
        u9.getListaCheckIn().add(c3);
        u9.addPontosCidade(c3, c3.getPontos());
        u9.getListaCheckIn().add(c8);
        u9.addPontosCidade(c8, c8.getPontos());

        //Adicionar amigos aos utilizadores
        u0.getListaAmigos().addUtilizador(u7);
        u0.getListaAmigos().addUtilizador(u4);
        u0.getListaAmigos().addUtilizador(u3);
        u1.getListaAmigos().addUtilizador(u6);
        u1.getListaAmigos().addUtilizador(u2);
        u1.getListaAmigos().addUtilizador(u8);
        u2.getListaAmigos().addUtilizador(u6);
        u2.getListaAmigos().addUtilizador(u7);
        u2.getListaAmigos().addUtilizador(u8);
        u2.getListaAmigos().addUtilizador(u1);
        u3.getListaAmigos().addUtilizador(u0);
        u3.getListaAmigos().addUtilizador(u8);
        u3.getListaAmigos().addUtilizador(u7);
        u3.getListaAmigos().addUtilizador(u6);
        u3.getListaAmigos().addUtilizador(u5);
        u4.getListaAmigos().addUtilizador(u9);
        u4.getListaAmigos().addUtilizador(u0);
        u5.getListaAmigos().addUtilizador(u8);
        u5.getListaAmigos().addUtilizador(u7);
        u5.getListaAmigos().addUtilizador(u6);
        u5.getListaAmigos().addUtilizador(u3);
        u5.getListaAmigos().addUtilizador(u9);
        u6.getListaAmigos().addUtilizador(u3);
        u6.getListaAmigos().addUtilizador(u5);
        u6.getListaAmigos().addUtilizador(u1);
        u6.getListaAmigos().addUtilizador(u2);
        u7.getListaAmigos().addUtilizador(u5);
        u7.getListaAmigos().addUtilizador(u3);
        u7.getListaAmigos().addUtilizador(u2);
        u7.getListaAmigos().addUtilizador(u0);
        u8.getListaAmigos().addUtilizador(u1);
        u8.getListaAmigos().addUtilizador(u2);
        u8.getListaAmigos().addUtilizador(u3);
        u8.getListaAmigos().addUtilizador(u5);
        u8.getListaAmigos().addUtilizador(u9);
        u9.getListaAmigos().addUtilizador(u5);
        u9.getListaAmigos().addUtilizador(u8);
        u9.getListaAmigos().addUtilizador(u4);

        //Adicionar os utilizadores ao resultado esperado
        r.addUtilizador(u0);
        r.addUtilizador(u1);
        r.addUtilizador(u2);
        r.addUtilizador(u3);
        r.addUtilizador(u4);
        r.addUtilizador(u5);
        r.addUtilizador(u6);
        r.addUtilizador(u7);
        r.addUtilizador(u8);
        r.addUtilizador(u9);

        LerFicheiro instance = new LerFicheiro(ru, rc, nomeFicheiro);
        instance.setNomeFicheiro(nomeFicheiroCidades);
        instance.carregarCidades();
        instance.setNomeFicheiro(nomeFicheiro);
        instance.carregarUtilizadores();

        assertEquals(r.getListaUtilizadores().toString(), ru.getListaUtilizadores().toString());
        int i = 0;
        for (Utilizador u : ru.getListaUtilizadores()) {

            //Compara a lista de check in
            assertEquals(u.getListaCheckIn().toString(), r.getListaUtilizadores().get(i).getListaCheckIn().toString());

            //Compara os pontos da cidade
            assertEquals(u.getListaCidades().toString(), r.getListaUtilizadores().get(i).getListaCidades().toString());

            //Compara a lista de amigos
            assertEquals(u.getListaAmigos().toString(), r.getListaUtilizadores().get(i).getListaAmigos().toString());
            i++;
        }
    }

    /**
     * Test of carregarUtilizadores method, of class LerFicheiro.
     */
    @Test
    public void testCarregarUtilizadores2() {
        System.out.println("carregarUtilizadores");
        String nomeFicheiroErrado = "users102";
        RegistoUtilizadores ru = new RegistoUtilizadores();
        RegistoCidades rc = new RegistoCidades();
        LerFicheiro lf = new LerFicheiro(ru, rc, nomeFicheiroErrado);
        lf.setNomeFicheiro("cities10");
        lf.carregarCidades();
        lf.setNomeFicheiro(nomeFicheiroErrado);
        System.out.println(lf.carregarUtilizadores());
        assertFalse(lf.carregarUtilizadores());

    }
    
}
