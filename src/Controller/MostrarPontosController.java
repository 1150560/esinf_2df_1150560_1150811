/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Utilizador;
import java.util.LinkedList;

/**
 *
 * @author NOVO
 */
public class MostrarPontosController {

    /**
     * Utilizador
     */
    private Utilizador u;

    /**
     * Constrói uma instância de mostrar pontos controller
     *
     * @param u - utilizador
     */
    public MostrarPontosController(Utilizador u) {
        this.u = u;
    }

    /**
     * Devolve o nº de pontos totais do utilizador
     *
     * @return nPontos
     */
    public int getPontosTotais() {
        return u.getPontosTotais();
    }

    /**
     * Devolve a lista de checkins
     *
     * @return listaCheckIn
     */
    public LinkedList getListaCheckIn() {
        return u.getListaCheckIn();
    }

}
