/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.CentroDados;
import Model.LerFicheiro;
import Model.RegistoCidades;
import Model.RegistoUtilizadores;

/**
 *
 * @author NOVO
 */
public class LerUtilizadoresController {

    /**
     * Registo Utilizadores
     */
    private RegistoUtilizadores ru;

    /**
     * Registo Cidades
     */
    private RegistoCidades rc;

    /**
     * Centro Utilizadores
     */
    private final CentroDados cd;

    /**
     * Constrói uma instância de LerUtilizadores dando como parametro o centro de dados
     *
     * @param cd
     */
    public LerUtilizadoresController(CentroDados cd) {
        this.cd = cd;
    }

    /**
     * Adiciona à lista de cidades as cidades que ler do ficheiro
     *
     * @param nomeFicheiro
     */
    public void registaUtilizadores(String nomeFicheiro) {
        this.ru = cd.getRegistoUtilizadores();
        this.rc = cd.getRegistoCidades();
        LerFicheiro lf = new LerFicheiro(ru, rc, nomeFicheiro);
        lf.carregarUtilizadores();
    }

}
