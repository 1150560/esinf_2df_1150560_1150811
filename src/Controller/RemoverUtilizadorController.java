/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;
import Model.*;

/**
 *
 * @author w8
 */
public class RemoverUtilizadorController {
    
    /**
     * Centro de dados
     */
    private final CentroDados cd;
    
    /**
     * Registo de utilizadores do centro de dados
     */
    private RegistoUtilizadores ru;
    
    /**
     * Lista de amigos
     */
    private RegistoUtilizadores lm;
    
    /**
     * Constrói uma instância de removerUtilizadorController
     * recebendo como parâmetro o centro de dados.
     * 
     * @param cd - centro de dados
     */
    public RemoverUtilizadorController(CentroDados cd){
        this.cd = cd;
    }
    
    /**
     * Remove o utilizador na lista de amigos de cada utilizador e no centro de dados
     * 
     * @param u - utilizador
     * @return return true caso remova o tuilizador, false se não remover
     */
    public boolean removerUtilizador(Utilizador u){
         this.lm = u.getListaAmigos();
         for(Utilizador u2: lm.getListaUtilizadores()){
             u2.removerAmigo(u);
         }
         
         this.ru = cd.getRegistoUtilizadores();
         return ru.removerUtilizador(u);
    }
    
}
