/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.CentroDados;
import Model.Cidade;
import java.util.Map;

/**
 *
 * @author NOVO
 */
public class MostrarCidadeController {

    /**
     * Centro de dados
     */
    private final CentroDados cd;

    /**
     * Construtor
     *
     * @param cd - Centro Dados
     */
    public MostrarCidadeController(CentroDados cd) {
        this.cd = cd;
    }

    /**
     * Mostra a lista de cidades ordenado por ordem decrescente dos pontos do mayor
     * 
     * @return map cidades -> pontos
     */
    public Map<Cidade, Integer> getListaCidades() {
        return cd.getRegistoCidades().ordenarCidades();
    }

}
