/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.*;

/**
 *
 * @author NOVO
 */
public class CheckInController {

    /**
     * Centro de dados
     */
    private final CentroDados cd;

    /**
     * Utilizador
     */
    private final Utilizador u;

    /**
     * Cidade atual
     */
    private Cidade cidadeAtual;

    /**
     * Cria uma instância de check in controller
     *
     * @param cd - centro de dados
     * @param u - utilizador
     */
    public CheckInController(CentroDados cd, Utilizador u) {
        this.cd = cd;
        this.u = u;
    }

    /**
     * Retorna a lista de cidades
     *
     * @return listacidades
     */
    public RegistoCidades getListaCidades() {
        cidadeAtual = u.getCidadeAtual();
        return cd.getRegistoCidades();
    }

    /**
     * Faz check in na cidade
     *
     * @param cNova - nova cidade
     */
    public boolean isCidadeAtual(Cidade cNova) {
        if (cNova.equals(cidadeAtual)) {
            return true;
        }
        int nPontos = cNova.getPontos();
        u.addPontosCidade(cNova, nPontos);
        u.setCidadeAtual(cNova);
        u.getListaCheckIn().add(cNova);
        cNova.atualizarMayor(cd.getRegistoUtilizadores(), cd.getRegistoMayors());
        return false;
    }

}
