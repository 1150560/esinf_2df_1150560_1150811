/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.CentroDados;
import Model.Utilizador;
import java.util.Map;

/**
 *
 * @author NOVO
 */
public class MostrarUtilizadoresInfluentesController {

    /**
     * Centro de dados
     */
    private final CentroDados cd;

    /**
     * Constrói uma instância de MostrarUtilizadoresInfluentesController
     *
     * @param cd - CentroDados
     */
    public MostrarUtilizadoresInfluentesController(CentroDados cd) {
        this.cd = cd;
    }

    /**
     * Retorna um map ordenado por ordem decrescente de numero de amigos que cada utilizador tem
     *
     * @return map de Utilizadores/nAmigos
     */
    public Map<Utilizador, Integer> getListaUtilizadoresInfluentes() {
        return cd.getRegistoUtilizadores().getListaUtilizadoresInfluentes();
    }

}
