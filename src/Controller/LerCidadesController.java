/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.CentroDados;
import Model.LerFicheiro;
import Model.RegistoCidades;

/**
 *
 * @author w8
 */
public class LerCidadesController {
    
    /**
     * Registo Cidades
     */
    private RegistoCidades rc;
    
    /**
     * Centro Cidades
     */
    private final CentroDados cd;
    
    /**
     * Constrói uma instância de LerCidades dando como parametro o centro de dados
     * @param cd 
     */
    public LerCidadesController(CentroDados cd){
        this.cd = cd;
    }
   
    
    /**
     * Adiciona à lista de cidades as cidades que ler do ficheiro
     * @param nomeFicheiro 
     */
    public void registaCidades(String nomeFicheiro){
        this.rc = cd.getRegistoCidades();
        LerFicheiro lf = new LerFicheiro(rc, nomeFicheiro);
        lf.carregarCidades();
    }
}
