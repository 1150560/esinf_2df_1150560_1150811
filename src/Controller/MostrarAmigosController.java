/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.CentroDados;
import Model.Cidade;
import Model.Utilizador;
import java.util.List;

/**
 *
 * @author NOVO
 */
public class MostrarAmigosController {

    /**
     * Centro de dados
     */
    private final CentroDados cd;
    
    /**
     * Utilizador
     */
    private final Utilizador u;

    /**
     * Constrói uma instância de mostrar amigos controller
     *
     * @param cd - Centro Dados
     * @param u - Utilizador
     */
    public MostrarAmigosController(CentroDados cd, Utilizador u) {
        this.cd = cd;
        this.u = u;
    }

    /**
     * Devolve a lista de cidades do centro
     * 
     * @return  lista de cidades
     */
    public List<Cidade> getListaCidades() {
        return cd.getRegistoCidades().getListaCidades();
    }
    
     /**
     * Devolve a lista de amigos da loocalizacao c
     * 
     * @param c - cidade
     * @return  lista de amigos
     */
    public List<Utilizador> getListaAmigosLocalizacao(Cidade c) {
       return u.getListaAmigos().getListaAmigosLocalizacao(c);
    }
    
    

}
