/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.CentroDados;
import Model.RegistoUtilizadores;
import Model.Utilizador;

/**
 *
 * @author NOVO
 */
public class CriarAmizadeController {

    /**
     * Centro Dados
     */
    private final CentroDados cd;

    /**
     * Registo de Utilizadores
     */
    private RegistoUtilizadores ru;

    /**
     * Utilizador principal
     */
    private final Utilizador user;

    /**
     * Utilizador a ser adicionado
     */
    private Utilizador userNovo;

    /**
     * Lista de amigos do utilizador principal
     */
    private RegistoUtilizadores la;

    /**
     * Lista de amigos do utilizador a ser adicionado
     */
    private RegistoUtilizadores la2;

    /**
     * Construtor
     *
     * @param cd - Centro Dados
     * @param user - utilizador que fez log in
     */
    public CriarAmizadeController(CentroDados cd, Utilizador user) {
        this.cd = cd;
        this.user = user;
    }

    /**
     * Adiciona às listas de amigos dos 2 utilizadores, o outro utilizador, caso ainda não sejam amigos
     *
     * @param nickname - nickname do utilizador que não efetoou log in
     */
    public boolean addAmigo(String nickname) {
        la = user.getListaAmigos();
        if (la.isUtilizador(nickname)) {
            return false;
        }
        ru = cd.getRegistoUtilizadores();
        userNovo = ru.getUtilizador(nickname);
        la.addUtilizador(userNovo);
        la2 = userNovo.getListaAmigos();
        la2.addUtilizador(user);
        return true;
    }

}
