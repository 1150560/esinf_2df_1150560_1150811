/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.RegistoUtilizadores;
import Model.Utilizador;

/**
 *
 * @author w8
 */
public class RemoverAmigoController {

    /**
     * Lista Amigos do utilizador
     */
    protected RegistoUtilizadores listaAmigos;

    /**
     * Utilizador
     */
    protected Utilizador u;

    /**
     * Contrói uma instância de removerAmigoController
     *
     * @param u - utilizador
     */
    public RemoverAmigoController(Utilizador u) {
        this.u = u;
    }

    /**
     * Atualiza a variavel lista amigos para a lista de amigos do utilizador dado por nparametro
     *
     *
     */
    public void getListaAmigos() {
        listaAmigos = u.getListaAmigos();
    }

    /**
     * Remove o utilizador 1 da lista de amigos do utilizador2 e vice-versa
     *
     * @param u2
     * @return
     */
    public boolean removeAmigos(Utilizador u2) {
        listaAmigos.getListaUtilizadores().remove(u2);
        return u2.getListaAmigos().getListaUtilizadores().remove(u);
    }

}
