/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.*;

/**
 *
 * @author w8
 */
public class CriarCidadeController {
    
    /**
     * Variavel Centro De Dados
     */
    private final CentroDados cd;
    
    /**
     * Variavel Cidade
     */
    protected Cidade c;
    
    /**
     * Cria uma instancia de CriarCidadeController
     * @param cd 
     */
    public CriarCidadeController(CentroDados cd){
        this.cd = cd;
    }
    
    /**
     * Cria uma nova cidade por omissao
     */
    public void novaCidade(){
       c = cd.getRegistoCidades().novoCidade();
        
    }
    /**
     * Modifica os dados da cidade caso passem nas validações
     * @param nome - nome
     * @param pontos - pontos
     * @param latitude -  latitude
     * @param longitude  - longitude
     */
    public void SetDados(String nome, int pontos, double latitude, double longitude){
        c.setNome(nome);
        c.setPontos(pontos);
        c.setLatitude(latitude);
        c.setLongitude(longitude);
    }
    
    /**
     * Regista a cidade caso não exista na lista de cidades
     * @return true se registar a cidade, caso contrario return false
     */
    public boolean registaCidade(){
       return cd.getRegistoCidades().registaCidade(c);
    }
}
