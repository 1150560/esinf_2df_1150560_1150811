/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.*;

/**
 *
 * @author NOVO
 */
public class CriarUtilizadorController {

    /**
     * Registo de utilizadores
     */
    private RegistoUtilizadores ru;

    /**
     * Centro de dados
     */
    private final CentroDados cd;

    /**
     * Utilizador
     */
    protected Utilizador user;

    /**
     * Construtor
     *
     * @param cd - Centro Dados
     */
    public CriarUtilizadorController(CentroDados cd) {
        this.cd = cd;
    }

    /**
     * Cria um novo utilizador
     */
    public void novoUtilizador() {
        ru = cd.getRegistoUtilizadores();
        user = ru.novoUtilizador();
    }

    /**
     * Modifica os dados
     *
     * @param nickname - novo nickname
     * @param email - novo email
     */
    public void setDados(String nickname, String email) {
        user.setNickname(nickname);
        user.setEmail(email);
    }

    /**
     * Registo o utilizador
     */
    public boolean registaUtilizador() {
        return ru.registaUtilizador(user);
    }

}
