/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Matriz.AdjacencyMatrixGraph;
import Matriz.EdgeAsDoubleGraphAlgorithms;
import Matriz.GraphAlgorithms;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author NOVO
 */
public class RegistoCidades {

    /**
     * A lista de cidades
     */
    private List<Cidade> listaCidades;

    /**
     * Constrói uma instância de RegistoCidades recebendo como parâmetro uma lista de cidades
     *
     * @param listaCidades - lista de cidades
     */
    public RegistoCidades(List<Cidade> listaCidades) {
        this.listaCidades = new LinkedList<>(listaCidades);
    }

    /**
     * Constrói uma instância de RegistoCidades recebendo como parâmetro um outro objeto RegistoCidades
     *
     * @param outroRegistoCidades - outro objeto RegistoCidades
     */
    public RegistoCidades(RegistoCidades outroRegistoCidades) {
        listaCidades = outroRegistoCidades.getListaCidades();
    }

    /**
     * Constrói uma instância de RegistoCidades
     */
    public RegistoCidades() {
        this.listaCidades = new LinkedList<Cidade>();
    }

    /**
     * Devolve a lista de cidades
     *
     * @return the listaCidades
     */
    public List<Cidade> getListaCidades() {
        return listaCidades;
    }

    /**
     * Modifica a lista de cidades
     *
     * @param listaCidades - nova lista de cidades
     */
    public void setListaCidades(List<Cidade> listaCidades) {
        this.listaCidades = new LinkedList<>(listaCidades);
    }

    /**
     * Devolve a descrição textual de RegistoCidades
     *
     * @return descricao textual RegistoCidades
     */
    @Override
    public String toString() {
        return "A lista de cidades é:\n" + listaCidadesToString();
    }

    /**
     * Modifica a forma como é apresentada a lista de cidades
     *
     * @return descricao textual RegistoCidades
     */
    public String listaCidadesToString() {
        StringBuilder s = new StringBuilder();
        for (Cidade c : listaCidades) {
            s.append(c);
            s.append("\n");
        }
        return s.toString();
    }

    /**
     * Cria um novo Cidade
     *
     * @return cidade
     */
    public Cidade novoCidade() {
        Cidade cidade = new Cidade();
        return cidade;
    }

    /**
     * Regista o cidade
     *
     * @param c - cidade
     * @return
     */
    public boolean registaCidade(Cidade c) {
        if (valida(c)) {
            addCidade(c);
            return true;
        }
        return false;
    }

    /**
     * Verifica se a cidade já existe na lista de cidades
     *
     * @param c - cidade
     */
    public boolean valida(Cidade c) {
        for (Cidade cidade : getListaCidades()) {
            if (cidade.getNome().equalsIgnoreCase(c.getNome())) {
                return false;
            }
        }
        return true;
    }

    /**
     * Adiciona um cidade à lista
     *
     * @param c - Cidade
     */
    public void addCidade(Cidade c) {
        listaCidades.add(c);
    }

    /**
     * Ordena o map de cidades->pontos, por ordem decrescente dos pontos
     *
     * @return map
     */
    public Map<Cidade, Integer> ordenarCidades() {

        Map<Cidade, Integer> listaC = new LinkedHashMap<>();

        //Preenche a listaC em que a key é C, value nPontos
        for (Cidade c : listaCidades) {
            listaC.put(c, c.getMayor().getPontosCidade(c.getNome()));
        }

        //Criacao de um array List de maps onde se vai ordenar por ordem decrescente de pontos
        List<Map.Entry<Cidade, Integer>> values = new ArrayList<Map.Entry<Cidade, Integer>>(listaC.entrySet());
        Collections.sort(values, new Comparator<Map.Entry<Cidade, Integer>>() {
            @Override
            public int compare(Map.Entry<Cidade, Integer> c1,
                    Map.Entry<Cidade, Integer> c2) {
                return c2.getValue() - c1.getValue();
            }
        });

        //Introduz no map as cidades e nPontos por ordem decrescente
        Map<Cidade, Integer> listaCFinal = new LinkedHashMap<>();
        for (Map.Entry<Cidade, Integer> entry : values) {
            listaCFinal.put(entry.getKey(), entry.getValue());
        }

        return listaCFinal;
    }

    /**
     * Devolve a cidade com o nome nomeCidade
     *
     * @param nomeCidade - nome da cidade
     * @return c
     */
    public Cidade getCidade(String nomeCidade) {
        for (Cidade c : listaCidades) {
            if (c.getNome().equalsIgnoreCase(nomeCidade)) {
                return c;
            }
        }
        return null;
    }

}
