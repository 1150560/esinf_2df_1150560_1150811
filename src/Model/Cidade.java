/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 *
 * @author NOVO
 */
public class Cidade implements Comparable<Cidade> {

    /**
     * Nome da cidade
     */
    private String nome;

    /**
     * Pontos que a cidade dá ao fazer check - in
     */
    private int pontos;

    /**
     * Latitude da cidade
     */
    private double latitude;

    /**
     * Longitude da cidade
     */
    private double longitude;

    /**
     * Mayor
     */
    private Utilizador mayor;

    /**
     * Lista de registo de utilizadores numa cidade
     */
    private Set<Utilizador> listaRegistos;

    /**
     * Nome por omissao
     */
    private static final String NOME_POR_OMISSAO = "sem nome";

    /**
     * Pontos por omissao da cidade
     */
    private static final int PONTOS_POR_OMISSAO = 0;

    /**
     * Latitude por omissão de uma cidade
     */
    private static final double LATITUDE_POR_OMISSAO = 0.0f;

    /**
     * Longitude por omissao de uma cidade
     */
    private static final double LONGITUDE_POR_OMISSAO = 0.0f;

    /**
     * Mayor por omissão
     */
    private static final Utilizador MAYOR_POR_OMISSAO = new Utilizador();

    /**
     * Lista de registos por omissão
     */
    private final Set<Utilizador> LISTA_REGISTOS_OMISSAO = new LinkedHashSet<>();

    /**
     * Constroi uma instancia de Cidade recebendo como parametro Nome, pontos
     * latitude e longitude da cidade
     *
     * @param nome - nome da cidade
     * @param pontos - pontos da cidade
     * @param latitude - latitude da cidade
     * @param longitude - longitude da cidade
     */
    public Cidade(String nome, int pontos, double latitude, double longitude) {
        setNome(nome);
        setPontos(pontos);
        setLatitude(latitude);
        setLongitude(longitude);
        this.mayor = MAYOR_POR_OMISSAO;
        this.listaRegistos = LISTA_REGISTOS_OMISSAO;
    }

    /**
     * Constrói uma instância de Cidade recebendo como parametro um objeto
     * Cidade
     *
     * @param outraCidade - Objeto cidade
     */
    public Cidade(Cidade outraCidade) {
        this.nome = outraCidade.nome;
        this.pontos = outraCidade.pontos;
        this.latitude = outraCidade.latitude;
        this.longitude = outraCidade.longitude;
        this.mayor = outraCidade.mayor;
        this.listaRegistos = outraCidade.listaRegistos;
    }

    /**
     * Constrói uma instância de Cidade
     */
    public Cidade() {
        this.nome = NOME_POR_OMISSAO;
        this.pontos = PONTOS_POR_OMISSAO;
        this.latitude = LATITUDE_POR_OMISSAO;
        this.longitude = LONGITUDE_POR_OMISSAO;
        this.mayor = MAYOR_POR_OMISSAO;
        this.listaRegistos = LISTA_REGISTOS_OMISSAO;
    }

    /**
     * @return o nome da cidade
     */
    public String getNome() {
        return nome;
    }

    /**
     * @return pontos da cidade
     */
    public int getPontos() {
        return pontos;
    }

    /**
     * @return the latitude da cidade
     */
    public double getLatitude() {
        return latitude;
    }

    /**
     * @return the longitude da cidade
     */
    public double getLongitude() {
        return longitude;
    }

    /**
     * @return the mayor
     */
    public Utilizador getMayor() {
        return mayor;
    }

    /**
     * Devolve a lista de registos
     *
     * @return the listaRegistos
     */
    public Set<Utilizador> getListaRegistos() {
        return listaRegistos;
    }

    /**
     * Modifica o nome da cidade atual pelo nome recebido por parametro
     *
     * @param nome - nome da cidade
     */
    public void setNome(String nome) {
        if (nome == null || nome.trim().isEmpty()) {
            throw new IllegalArgumentException("Nome inválido!");
        }
        this.nome = nome;
    }

    /**
     * Modifica os pontos da cidade pelos pontos recebido por parametro
     *
     * @param pontos the pontos to set
     */
    public void setPontos(int pontos) {
        if (pontos <= 0) {
            throw new IllegalArgumentException("Pontos inválidos");
        }
        this.pontos = pontos;
    }

    /**
     * @param latitude the latitude to set
     */
    public void setLatitude(double latitude) {
        if (latitude < -90 || latitude > 90) {
            throw new IllegalArgumentException("Latitude inválida");
        }
        this.latitude = latitude;
    }

    /**
     * @param longitude the longitude to set
     */
    public void setLongitude(double longitude) {
        if (longitude < -180 || longitude > 180) {
            throw new IllegalArgumentException("Longitude inválida");
        }
        this.longitude = longitude;
    }

    /**
     * @param mayor the mayor to set
     */
    public void setMayor(Utilizador mayor) {
        this.mayor = mayor;
    }
    
     /**
     * Modifica a lista de utilizadores da cidade, pela dada como parametro
     * @param listaRegistos - lista de utilizadores
     */
    public void setListaRegistos(Set<Utilizador> listaRegistos) {
        this.listaRegistos = listaRegistos;
    }

    /**
     * Devolve a descrição textual da cidade
     *
     * @return descricao textual da cidade
     */
    @Override
    public String toString() {
        return "[" + nome + "]";
    }

    /**
     * Adicionar um utilizador que foi regista
     *
     * @param u - Utilizador
     * @return true se registar
     */
    public boolean registarUtilizador(Utilizador u) {
        return getListaRegistos().add(u);
    }

    /**
     * Verifica se a cidade recebido como parâmetro é igual à atual
     *
     * @param cidade - cidade
     * @return true se forem iguais, false se não forem
     */
    public boolean equals(Cidade cidade) {
        if (this == cidade) {
            return true;
        }
        if (cidade == null || getClass() != cidade.getClass()) {
            return false;
        }
        Cidade outraCidade = (Cidade) cidade;
        return nome.equalsIgnoreCase(outraCidade.nome) && pontos == outraCidade.pontos
                && latitude == outraCidade.latitude && longitude == outraCidade.longitude;
    }

    /**
     * Atualiza o maior, recebendo como parametro a lista de utilizadores
     *
     * @param ru - Lista de utilizadores
     */
    public void atualizarMayor(RegistoUtilizadores ru, RegistoMayors rm) {
        for (Utilizador u : ru.getListaUtilizadores()) {
            if (mayor.getPontosCidade(nome) < u.getPontosCidade(nome)) {
                mayor = u;
            }
        }
        Mayor m = new Mayor(mayor, this, mayor.getPontosCidade(nome));
        if (!rm.registaMayor(m)) {
            rm.setMayorCidade(m);
        }
    }

    /**
     * Compare to da cidade, verificando pelo numero de registos que cada cidade
     * tem
     *
     * @param c - cidade
     * @return
     */
    @Override
    public int compareTo(Cidade c) {
        if (this.listaRegistos.size() - c.listaRegistos.size() != 0) {
            return this.listaRegistos.size() - c.listaRegistos.size();
        }
        return this.getNome().compareTo(c.getNome());
    }

}
