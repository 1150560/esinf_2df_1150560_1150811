/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Matriz.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author w8
 */
public class LerFicheiro {

    /**
     * Registo cidades
     */
    private RegistoCidades rc;

    /**
     * Registo utilizadores
     */
    private RegistoUtilizadores ru;

    /**
     * matriz de adjacencias
     */
    private AdjacencyMatrixGraph<Cidade, Double> adjacencyMatrix;

    /**
     * Nome do ficheiro
     */
    private String nomeFicheiro;

    /**
     * Registo de cidades por omissão
     */
    private final RegistoCidades REGISTO_CIDADES_OMISSAO = new RegistoCidades();

    /**
     * Registo Utilizadores por omissão
     */
    private final RegistoUtilizadores REGISTO_UTILIZADORES_OMISSAO = new RegistoUtilizadores();

    /**
     * matriz de adjacencias por omissao
     */
    private final AdjacencyMatrixGraph<Cidade, Double> GRAPH_OMISSAO = new AdjacencyMatrixGraph<Cidade, Double>();

    /**
     * Extensao txt
     */
    private static final String EXTENSAO_TXT = ".txt";


    /**
     * Constrói uma instância de LerFicheiro dando como parametro o registo de cidades e nome do ficheiro
     *
     * @param rc - registo cidades
     * @param nomeFicheiro - nome do ficheiro
     */
    public LerFicheiro(RegistoCidades rc, String nomeFicheiro) {
        this.rc = rc;
        this.nomeFicheiro = nomeFicheiro + EXTENSAO_TXT;
        this.ru = REGISTO_UTILIZADORES_OMISSAO;
        this.adjacencyMatrix = GRAPH_OMISSAO;
    }

    /**
     * Constrói uma instância de LerFicheiro dando como parametro o registo de cidades e nome do ficheiro
     *
     * @param ru - registo Utilizadores
     * @param rc - registo Cidades
     * @param nomeFicheiro - nome do ficheiro
     */
    public LerFicheiro(RegistoUtilizadores ru, RegistoCidades rc, String nomeFicheiro) {
        this.ru = ru;
        this.rc = rc;
        this.nomeFicheiro = nomeFicheiro + EXTENSAO_TXT;
        this.adjacencyMatrix = GRAPH_OMISSAO;
    }

    /**
     * Devolve o registo de cidades
     *
     * @return registo cidades
     */
    public RegistoCidades getRc() {
        return rc;
    }

    /**
     * Devolve o registo de utilizadores
     *
     * @return ru
     */
    public RegistoUtilizadores getRu() {
        return ru;
    }

    /**
     * Devolve o nome do ficheiro
     *
     * @return the nomeFicheiro
     */
    public String getNomeFicheiro() {
        return nomeFicheiro;
    }

    /**
     * Modifica o registo de cidades
     *
     * @param rc the rc to set
     */
    public void setRc(RegistoCidades rc) {
        this.rc = rc;
    }

    /**
     * Modifica o registo de utilizadores
     *
     * @param ru the ru to set
     */
    public void setRu(RegistoUtilizadores ru) {
        this.ru = ru;
    }

    /**
     * Modifica o nome do ficheiro ´
     *
     * @param nomeFicheiro the nomeFicheiro to set
     */
    public void setNomeFicheiro(String nomeFicheiro) {
        this.nomeFicheiro = nomeFicheiro + EXTENSAO_TXT;
    }

    /**
     * Adiciona as cidades lidas no ficheiro ao registo de cidades
     */
    public boolean carregarCidades() {
        try {
            Scanner ler = new Scanner(new File(nomeFicheiro));
            try {
                while (ler.hasNextLine()) {
                    String linha = ler.nextLine();
                    if (!linha.isEmpty()) {
                        String[] vec = linha.split(",");
                        rc.registaCidade(new Cidade(vec[0], Integer.parseInt(vec[1]),
                                Double.parseDouble(vec[2]), Double.parseDouble(vec[3])));
                    }
                }
            } finally {
                ler.close();
            }
        } catch (FileNotFoundException e) {
            System.out.println("Ficheiro não encontrado!");
            return false;
        } catch (NumberFormatException a) {
            System.out.println("Erro na leitura");
            return false;
        }
        return true;
    }

    /**
     * Adiciona os utilizadores lidos no ficheiro ao registo de utilizadores
     */
    public boolean carregarUtilizadores() {
        try {
            Scanner ler = new Scanner(new File(nomeFicheiro));
            try {
                int nLinha = 0;

                //Cria os utilizadores e adiciona-os ao registo
                while (ler.hasNextLine()) {
                    nLinha++;
                    String linha = ler.nextLine();
                    if (!linha.isEmpty()) {

                        if (nLinha % 2 != 0) {
                            Utilizador u = new Utilizador();
                            String[] vec = linha.split(",");
                            u.setNickname(vec[0]);
                            u.setEmail(vec[1]);
                            for (int i = 2; i < vec.length; i++) {
                                Cidade c = rc.getCidade(vec[i]);
                                u.getListaCheckIn().add(c);
                                u.addPontosCidade(c, c.getPontos());
                                c.registarUtilizador(u);
                                if (i == vec.length - 1) {
                                    u.setCidadeAtual(c);
                                }
                            }
                            ru.registaUtilizador(u);
                        }
                    }
                }
                Scanner ler2 = new Scanner(new File(nomeFicheiro));
                nLinha = 0;
                Utilizador u = new Utilizador();

                //Adiciona aos utilizadores criados, os amigos
                while (ler2.hasNextLine()) {
                    nLinha++;
                    String linha = ler2.nextLine();
                    if (!linha.isEmpty()) {

                        if (nLinha % 2 != 0) {
                            String[] vec = linha.split(",");
                            u.setNickname(vec[0]);
                            u.setEmail(vec[1]);

                        }
                        if (nLinha % 2 == 0) {
                            String[] vec = linha.split(",");
                            for (int i = 0; i < vec.length; i++) {
                                Utilizador u2 = ru.getUtilizador(vec[i]);
                                ru.getUtilizador(u.getNickname()).getListaAmigos().addUtilizador(u2);
                            }
                        }
                    }
                }

            } finally {
                ler.close();
            }
        } catch (FileNotFoundException e) {
            System.out.println("Ficheiro não encontrado!");
            return false;
        } catch (Exception a) {
            System.out.println("Erro na leitura");
            return false;
        }
        return true;
    }

    
}
