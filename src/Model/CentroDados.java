/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Graph.Graph;
import Graph.Vertex;
import Matriz.AdjacencyMatrixGraph;

/**
 *
 * @author NOVO
 */
public class CentroDados {

    /**
     * Variavel registo utilizadores que contem uma lista de utilizadores
     */
    private RegistoUtilizadores registoUtilizadores;

    /**
     * Variavel registo cidades que contem uma lista de cidades
     */
    private RegistoCidades registoCidades;

    /**
     * Registo de Mayors
     */
    private RegistoMayors registoMayors;

    /**
     * Matriz de adjacencias
     */
    private MatrizAdjacencias AdjacencyMatrix;

    /**
     * Mapa de adjacencias
     */
    private MapAdjacencias mapAdjacencias;

    /**
     * Variavel registo utilizadores que contem uma lista de utilizadores por omissão
     */
    private final RegistoUtilizadores REGISTO_UTILIZADORES_OMISSAO = new RegistoUtilizadores();

    /**
     * Variavel registo cidades que contem uma lista de cidades por omissão
     */
    private final RegistoCidades REGISTO_CIDADES_OMISSAO = new RegistoCidades();

    /**
     * registo de mayors por omissao
     */
    private final RegistoMayors REGISTO_MAYORS_OMISSAO = new RegistoMayors();

    /**
     * Matriz de adjacencias por omissao
     */
    private final MatrizAdjacencias ADJACENCY_MATRIX_OMISSAO = new MatrizAdjacencias();

    /**
     * Map de adjacencias
     */
    private final MapAdjacencias MAP_ADJACENCIAS_OMISSAO = new MapAdjacencias();

    /**
     * Constrói uma instância de CentroDados
     */
    public CentroDados() {
        this.registoUtilizadores = REGISTO_UTILIZADORES_OMISSAO;
        this.registoCidades = REGISTO_CIDADES_OMISSAO;
        this.AdjacencyMatrix = ADJACENCY_MATRIX_OMISSAO;
        this.mapAdjacencias = MAP_ADJACENCIAS_OMISSAO;
        this.registoMayors = REGISTO_MAYORS_OMISSAO;
    }

    /**
     * Constrói uma instância de CentroDados recebendo como parâmetro um objeto registo utilizadores e um registo cidades
     *
     * @param registoUtilizadores - registoUtilizadores
     * @param registoCidades - registoCidades
     */
    public CentroDados(RegistoUtilizadores registoUtilizadores, RegistoCidades registoCidades) {
        this.registoUtilizadores = registoUtilizadores;
        this.registoCidades = registoCidades;
        this.registoMayors = REGISTO_MAYORS_OMISSAO;
        this.AdjacencyMatrix = ADJACENCY_MATRIX_OMISSAO;
        this.mapAdjacencias = MAP_ADJACENCIAS_OMISSAO;
    }

    /**
     * Devolve o registo de utilizadores
     *
     * @return the registoUtilizadores
     */
    public RegistoUtilizadores getRegistoUtilizadores() {
        return registoUtilizadores;
    }

    /**
     * Devolve o registo de cidades
     *
     * @return the registoCidades
     */
    public RegistoCidades getRegistoCidades() {
        return registoCidades;
    }

    /**
     * Devolve a lista de Mayors
     *
     * @return the registoMayors
     */
    public RegistoMayors getRegistoMayors() {
        return registoMayors;
    }

    /**
     * @return the AdjacencyMatrix
     */
    public MatrizAdjacencias getAdjacencyMatrix() {
        return AdjacencyMatrix;
    }

    /**
     * Devolve o map de adjacencias ´
     *
     * @return the mapAdjacencias
     */
    public MapAdjacencias getMapAdjacencias() {
        return mapAdjacencias;
    }

    /**
     * Modifica o registo de utilizadores
     *
     * @param registoUtilizadores - novo registo de utilizadores
     */
    public void setRegistoUtilizadores(RegistoUtilizadores registoUtilizadores) {
        this.registoUtilizadores = registoUtilizadores;
    }

    /**
     * Modifica o registo de cidades
     *
     * @param registoCidades - novo registo de cidades
     */
    public void setRegistoCidades(RegistoCidades registoCidades) {
        this.registoCidades = registoCidades;
    }

    /**
     * Modifica a lista de mayors
     * 
     * @param registoMayors the registoMayors to set
     */
    public void setRegistoMayors(RegistoMayors registoMayors) {
        this.registoMayors = registoMayors;
    }

    /**
     * Modifica a matriz de adjacencias ´
     *
     * @param AdjacencyMatrix the AdjacencyMatrix to set
     */
    public void setAdjacencyMatrix(MatrizAdjacencias AdjacencyMatrix) {
        this.AdjacencyMatrix = AdjacencyMatrix;
    }

    /**
     * Modifica o map de adjacencias pelo que se recebe por parametro
     *
     * @param mapAdjacencias the mapAdjacencias to set
     */
    public void setMapAdjacencias(MapAdjacencias mapAdjacencias) {
        this.mapAdjacencias = mapAdjacencias;
    }

}
