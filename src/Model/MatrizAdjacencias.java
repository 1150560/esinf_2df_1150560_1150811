/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Graph.Edge;
import Graph.Graph;
import Graph.Vertex;
import Matriz.AdjacencyMatrixGraph;
import Matriz.EdgeAsDoubleGraphAlgorithms;
import Matriz.GraphAlgorithms;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 *
 * @author w8
 */
public class MatrizAdjacencias {

    /**
     * Matriz de adjacencias
     */
    private AdjacencyMatrixGraph<Cidade, Double> matrizAdjacencias;

    /**
     * Constrói uma instância de matriz de adjacencias
     */
    public MatrizAdjacencias() {
        this.matrizAdjacencias = new AdjacencyMatrixGraph<>();
    }

    /**
     * Devolve a matriz de adjacencias
     *
     * @return the matrizAdjacencias
     */
    public AdjacencyMatrixGraph<Cidade, Double> getMatrizAdjacencias() {
        return matrizAdjacencias;
    }

    /**
     * Modifica a matriz de adjacencias pela dada como parametro
     *
     * @param matrizAdjacencias the matrizAdjacencias to set
     */
    public void setMatrizAdjacencias(AdjacencyMatrixGraph<Cidade, Double> matrizAdjacencias) {
        this.matrizAdjacencias = matrizAdjacencias;
    }

    /**
     * Adiciona as ligações entre as cidades na matriz de adjacencias
     *
     * @param nomeFicheiro - nome do ficheiro
     * @param rc - registo Cidades
     * @return true se carregar as conecções
     */
    public boolean carregarConexoes(String nomeFicheiro, RegistoCidades rc) {
        nomeFicheiro = nomeFicheiro + ".txt";
        try {
            Scanner ler = new Scanner(new File(nomeFicheiro));
            try {
                while (ler.hasNextLine()) {
                    String linha = ler.nextLine();
                    if (!linha.isEmpty()) {
                        String[] vec = linha.split(",");
                        matrizAdjacencias.insertVertex(rc.getCidade(vec[0]));
                        matrizAdjacencias.insertVertex(rc.getCidade(vec[1]));
                        matrizAdjacencias.insertEdge(rc.getCidade(vec[0]), rc.getCidade(vec[1]), Double.parseDouble(vec[2]));
                    }
                }
            } finally {
                ler.close();
            }
        } catch (FileNotFoundException e) {
            System.out.println("Ficheiro não encontrado!");
            return false;
        } catch (NumberFormatException a) {
            System.out.println("Erro na leitura");
            return false;
        }
        return true;
    }

    /**
     * Devolve a lista de amigos que estão a certa distancia, recebida por parametro
     *
     * @param map - map de adjacencias
     * @param u - utilizador
     * @param distancia - distancia
     * @return lista de amigos
     */
    public List<Utilizador> amigosProximos(MapAdjacencias map, Utilizador u, double distancia) {
        List<Utilizador> listaAmigosDist = new LinkedList<>();
        List<Utilizador> listaAmigos = map.listaAmigos(u);

        for (Utilizador amigo : listaAmigos) {
            if (EdgeAsDoubleGraphAlgorithms.shortestPath(matrizAdjacencias, u.getCidadeAtual(),
                    amigo.getCidadeAtual(), new LinkedList<>()) < distancia) {
                listaAmigosDist.add(amigo);
            }
        }

        return listaAmigosDist;
    }

    /**
     * Retorna o caminho mais curto entre 2 utilizadores
     *
     * @param u1 - Utilizador1
     * @param u2 - Utilizador2
     * @return caminhoMaisCurto
     */
    public Map<LinkedList<Cidade>, Double> caminhoMaisCurto(Utilizador u1, Utilizador u2) {

        Cidade c1 = u1.getCidadeAtual();
        Cidade c2 = u2.getCidadeAtual();

        LinkedList<Cidade> lista = new LinkedList<>();
        double distancia = EdgeAsDoubleGraphAlgorithms.shortestPath(matrizAdjacencias, c1, c2, lista);

        Map<LinkedList<Cidade>, Double> map = new LinkedHashMap<>();
        map.put(lista, distancia);
        return map;
    }

    /**
     * Retorna o numero de amigos que um utilizador tem numa certa cidade
     *
     * @param listaAmigos - lista de amigos
     * @param c - cidade
     * @return nº de amigos
     */
    public int nAmigosCidade(List<Utilizador> listaAmigos, Cidade c) {
        int nAmigos = 0;
        for (Utilizador u : listaAmigos) {
            if (u.getCidadeAtual().equals(c)) {
                nAmigos++;
            }
        }
        return nAmigos;
    }

    /**
     * Retorna Lista de cidades onde o utilizador tem mais Amigos
     *
     * @param listaAmigos
     * @return
     */
    public List<Cidade> getListaCidadesMaisAmigos(List<Utilizador> listaAmigos) {
        int nAmigosMaior = 0, nAmigosAtual = 0;
        List<Cidade> lista = new LinkedList<>();

        for (Cidade c : matrizAdjacencias.vertices()) {
            nAmigosAtual = nAmigosCidade(listaAmigos, c);
            if (nAmigosMaior < nAmigosAtual) {
                nAmigosMaior = nAmigosAtual;
                lista.clear();
            }

            if (nAmigosMaior == nAmigosAtual) {
                lista.add(c);
            }
        }
        return lista;
    }

    /**
     * Retorna o tamanho do caminho
     *
     * @param lista - caminho mais curto
     * @return tamanho
     */
    public int tamanhoCaminho(List<Cidade> lista) {
        int tamanho = 0;

        for (int i = 0; i < lista.size(); i++) {
            if (i != lista.size() - 1) {
                tamanho += matrizAdjacencias.getEdge(lista.get(i), lista.get(i + 1));
            }
        }
        return tamanho;
    }

    /**
     * Devolve o caminho minimo passado pelas cidades obrigatorias
     * 
     * @param map - map de utilizadores
     * @param u1 - utilizador1
     * @param u2 - utilizador2
     * @return listaCidades
     */
    public List<Cidade> caminhoMinObrigatorio(MapAdjacencias map, Utilizador u1, Utilizador u2) {

        int tamanhoCurto = -1, tamanhoAtual;
        List<Utilizador> listaAmigosU1 = map.listaAmigos(u1);
        List<Utilizador> listaAmigosU2 = map.listaAmigos(u2);
        List<Cidade> l1 = getListaCidadesMaisAmigos(listaAmigosU1);
        List<Cidade> l2 = getListaCidadesMaisAmigos(listaAmigosU2);

        List<Cidade> listaCidade = new LinkedList<>();
        LinkedList<LinkedList<Cidade>> listCaminhos = new LinkedList<>();
        LinkedList<LinkedList<Cidade>> allPath = new LinkedList<>();
        GraphAlgorithms.allPaths(matrizAdjacencias, u1.getCidadeAtual(), u2.getCidadeAtual(), allPath);

        for (LinkedList<Cidade> lista : allPath) {
            if (lista.containsAll(l1) && lista.containsAll(l2)) {
                listCaminhos.add(lista);
            }
        }

        for (List<Cidade> lista : listCaminhos) {
            tamanhoAtual = tamanhoCaminho(lista);

            if (tamanhoCurto == -1) {
                tamanhoCurto = tamanhoAtual;
                listaCidade = lista;
            }

            if (tamanhoCurto > tamanhoAtual) {
                tamanhoCurto = tamanhoAtual;
                listaCidade = lista;
            }
        }

        return listaCidade;
    }
}
