/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author NOVO
 */
public class Mayor implements Comparable<Mayor> {

    /**
     * Utilizador
     */
    private Utilizador utilizador;

    /**
     * Cidade
     */
    private Cidade cidade;

    /**
     * Pontos
     */
    private int pontos;

    /**
     * Constrói uma instância de Mayor
     */
    public Mayor() {
        utilizador = new Utilizador();
        cidade = new Cidade();
        pontos = 0;
    }

    /**
     * Constrói uma instância de Mayor recebenco como parametro o utilizador, cidade, int
     *
     * @param u - utilizador
     * @param c - cidade
     * @param p - pontos
     */
    public Mayor(Utilizador u, Cidade c, int p) {
        this.utilizador = u;
        this.cidade = c;
        this.pontos = p;
    }

    /**
     * Devolve o utilizador
     *
     * @return the utilizador
     */
    public Utilizador getUtilizador() {
        return utilizador;
    }

    /**
     * Devolve a cidade
     *
     * @return the cidade
     */
    public Cidade getCidade() {
        return cidade;
    }

    /**
     * Devolve os pontos
     *
     * @return the pontos
     */
    public int getPontos() {
        return pontos;
    }

    /**
     * Modifica o utilizador
     *
     * @param utilizador the utilizador to set
     */
    public void setUtilizador(Utilizador utilizador) {
        this.utilizador = utilizador;
    }

    /**
     * Modifica a cidade
     *
     * @param cidade the cidade to set
     */
    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }

    /**
     * Modifica os pontos
     *
     * @param pontos the pontos to set
     */
    public void setPontos(int pontos) {
        this.pontos = pontos;
    }

    /**
     * Devolve a descrição textual de Utilizador
     *
     * @return descricao textual Utilizador
     */
    @Override
    public String toString() {
        return String.format("[%s]", pontos);
    }

    @Override
    public int compareTo(Mayor t) {
        if (t.getPontos() - this.getPontos() == 0) {
            return t.hashCode() - this.hashCode();
        }
        return this.getPontos() - t.getPontos();
    }

}
