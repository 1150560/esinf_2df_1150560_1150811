/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author NOVO
 */
public class ArvoreBinaria {

    /**
     * Arvore Binaria de Utilizadores
     */
    private BST<Mayor> arvoreUtilizador;

    /**
     * Arvore Binaria de Utilizadores
     */
    private BST<Cidade> arvoreCidades;

    /**
     * Árvore de Utilizadores por omissão
     */
    private final BST<Mayor> ARVORE_UTILIZADOR_OMISSAO = new BST<>();

    /**
     * Árvore de Cidades por omissão
     */
    private final BST<Cidade> ARVORE_CIDADES_OMISSAO = new BST<>();

    /**
     * Constrói uma instância de ArvoreBinaria
     */
    public ArvoreBinaria() {
        this.arvoreUtilizador = ARVORE_UTILIZADOR_OMISSAO;
        this.arvoreCidades = ARVORE_CIDADES_OMISSAO;
    }

    /**
     * Devolve a arvore de utilizadores
     *
     * @return the arvoreUtilizador
     */
    public BST<Mayor> getArvoreUtilizador() {
        return arvoreUtilizador;
    }

    /**
     * devolve a arvore de cidades
     *
     * @return the arvoreCidades
     */
    public BST<Cidade> getArvoreCidades() {
        return arvoreCidades;
    }

    /**
     * Modifica a arvore de Utilizadores
     *
     * @param arvoreUtilizador the arvoreUtilizador to set
     */
    public void setArvoreUtilizador(BST<Mayor> arvoreUtilizador) {
        this.arvoreUtilizador = arvoreUtilizador;
    }

    /**
     * Modifica a arvore de cidades
     *
     * @param arvoreCidades the arvoreCidades to set
     */
    public void setArvoreCidades(BST<Cidade> arvoreCidades) {
        this.arvoreCidades = arvoreCidades;
    }

    /**
     * Cria a arvore de utilizadores, devolvendo os mayores por ordem
     * descrescente dos seus pontos
     *
     * @param rm - lista de mayores
     * @return lista de ayores
     */
    public List<Mayor> criarArvoreUtilizadores(List<Mayor> rm) {
        for (Mayor m : rm) {
            if (m.getPontos() > 0) {
                arvoreUtilizador.insert(m);
            }
        }
        Iterable<Mayor> it = arvoreUtilizador.inInverseOrder();
        List<Mayor> lista = new LinkedList<>();
        for (Mayor m : it) {
            lista.add(m);
        }
        System.out.println("Ordem decrescente dos pontos dos Mayors: " + lista);
        return lista;
    }
    
    /**
     * Cria a arvore de Cidades, apresentado as cidades por ordem crescente de 
     * utilizadores registados
     *
     * @param lc - lista de cidades
     * @return lista de cidades
     */
    public List<Cidade> criarArvoreCidades(List<Cidade> lc) {
        for (Cidade c : lc) {
            arvoreCidades.insert(c);
        }
        Iterable<Cidade> lista = arvoreCidades.inOrder();
        List<Cidade> listaF = new LinkedList<>();
        System.out.println("Lista de cidades por ordem crescente de registos");
        for (Cidade c: lista) {
            listaF.add(c);
            System.out.printf("[Cidade %s: %d registos]%n", c.getNome(), c.getListaRegistos().size());
        }
        return listaF;
    }
}
