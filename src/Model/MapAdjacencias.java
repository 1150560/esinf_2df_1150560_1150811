/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Graph.Edge;
import Graph.Graph;
import Graph.GraphAlgorithm;
import Graph.Vertex;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author NOVO
 */
public class MapAdjacencias {

    /**
     * grafico de utilizadores
     */
    private Graph<Utilizador, Vertex> map;

    /**
     * grafico de utilizadores por omissao
     */
    private final Graph<Utilizador, Vertex> MAP_OMISSAO = new Graph<>(false);

    /**
     * Constrói uma instância de map adjacencias
     */
    public MapAdjacencias() {
        this.map = MAP_OMISSAO;

    }

    /**
     * Retorna o map de utilizadores
     *
     * @return the map
     */
    public Graph<Utilizador, Vertex> getMap() {
        return map;
    }

    /**
     * Modifica o map de utilizadores
     *
     * @param map the map to set
     */
    public void setMap(Graph<Utilizador, Vertex> map) {
        this.map = map;
    }

    /**
     * Contrução do mapa de adjacências a partir do contentor lista de utilizadores
     *
     */
    public void mapAdjacencias(List<Utilizador> listaUtilizadores) {
        for (Utilizador u : listaUtilizadores) {
            for (Utilizador amigo : u.getListaAmigos().getListaUtilizadores()) {
                if (getMap().getEdge(u, amigo) == null) {
                    getMap().insertEdge(u, amigo, null, 1);
                }

            }
        }
    }

    /**
     * Retorna os amigos a uma distância inferior a d
     *
     * @param d - distancia
     * @param principal - Utilizador
     * @return lista
     */
    public List<Utilizador> amigosDistancia(Utilizador principal, int d) {

        List<Utilizador> lista = new LinkedList<>();

        Object[] listaObject = map.allkeyVerts();
        for (int i = 0; i < listaObject.length; i++) {
            Utilizador u = ((Utilizador) listaObject[i]);
            if (!u.equals(principal)) {
                if (GraphAlgorithm.shortestPath(getMap(), principal, u, new LinkedList<>()) <= d) {
                    lista.add(u);
                }
            }
        }
        return lista;
    }

    /**
     * Retorna a distancia entre 2 utilizadores
     *
     * @param u1 - Utilizador1
     * @param u2 - Utilizador2
     * @return lista
     */
    public double distancia2Utilizadores(Utilizador u1, Utilizador u2) {
        return GraphAlgorithm.shortestPath(getMap(), u1, u2, new LinkedList<>()) - 1;
    }

    /**
     * Retorna os utilizadores mais influentes
     *
     * @return Lista Utilizadores mais influentes
     */
    public List<Utilizador> utilizadoresMaisInfluentes() {

        double maiorCaminho = 0, caminhoAtual, menorCaminho = -1;
        List<Utilizador> lista = new LinkedList<>();

        Object[] listaObject = map.allkeyVerts();
        for (int i = 0; i < listaObject.length; i++) {
            Utilizador u1 = ((Utilizador) listaObject[i]);
            //Para cada utilizador, calcular a centralidade
            for (int j = 0; j < listaObject.length; j++) {
                Utilizador u2 = ((Utilizador) listaObject[j]);
                if (!u1.equals(u2)) {
                    caminhoAtual = GraphAlgorithm.shortestPath(map, u1, u2, new LinkedList<>());
                    if (maiorCaminho < caminhoAtual) {
                        maiorCaminho = caminhoAtual;
                    }
                }
            }
            //adicionar os utilizadores, com menor centralidade
            if (menorCaminho == -1) {
                menorCaminho = maiorCaminho;
                lista.add(u1);
            }

            if (menorCaminho > maiorCaminho) {
                menorCaminho = maiorCaminho;
                lista.clear();
            }

            if (menorCaminho == maiorCaminho) {
                lista.add(u1);
            }
            maiorCaminho = 0;
        }

        return lista;
    }

    /**
     * Devolve a lista de amigos de 1 utilizador
     *
     * @param u - utilizador
     * @return listaAmigos
     */
    public List<Utilizador> listaAmigos(Utilizador u) {
        List<Utilizador> listaAmigos = new LinkedList<>();
        Iterable<Edge<Utilizador, Vertex>> iterableEdges = map.outgoingEdges(u);
        for (Edge e : iterableEdges) {
            listaAmigos.add((Utilizador) e.getVDest());
        }
        return listaAmigos;
    }

}
