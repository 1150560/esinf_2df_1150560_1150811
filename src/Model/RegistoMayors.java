/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author NOVO
 */
public class RegistoMayors {

    /**
     * A lista de mayors
     */
    private List<Mayor> listaMayors;

    /**
     * Constrói uma instância de RegistoMayors recebendo como parâmetro uma lista de mayors
     *
     * @param listaMayors - lista de mayors
     */
    public RegistoMayors(List<Mayor> listaMayors) {
        this.listaMayors = new LinkedList<>(listaMayors);
    }

    /**
     * Constrói uma instância de RegistoMayors recebendo como parâmetro um outro objeto RegistoMayors
     *
     * @param outroRegistoMayors - outro objeto RegistoMayors
     */
    public RegistoMayors(RegistoMayors outroRegistoMayors) {
        listaMayors = outroRegistoMayors.getListaMayors();
    }

    /**
     * Constrói uma instância de RegistoMayors
     */
    public RegistoMayors() {
        this.listaMayors = new LinkedList<Mayor>();
    }

    /**
     * Devolve a lista de mayors
     *
     * @return the listaMayors
     */
    public List<Mayor> getListaMayors() {
        return listaMayors;
    }

    /**
     * Modifica a lista de mayors
     *
     * @param listaMayors - nova lista de mayors
     */
    public void setListaMayors(List<Mayor> listaMayors) {
        this.listaMayors = new LinkedList<>(listaMayors);
    }

    /**
     * Devolve a descrição textual de RegistoMayors
     *
     * @return descricao textual RegistoMayors
     */
    @Override
    public String toString() {
        return "A lista de mayors é:\n" + listaMayorsToString();
    }

    /**
     * Modifica a forma como é apresentada a lista de mayors
     *
     * @return descricao textual RegistoMayors
     */
    public String listaMayorsToString() {
        StringBuilder s = new StringBuilder();
        for (Mayor m : listaMayors) {
            s.append(m);
            s.append("\n");
        }
        return s.toString();
    }

    /**
     * Regista o mayor
     *
     * @param m - mayor
     * @return
     */
    public boolean registaMayor(Mayor m) {
        if (valida(m)) {
            addMayor(m);
            return true;
        }
        return false;
    }

    /**
     * Verifica se a mayor já existe na lista de mayors
     *
     * @param m - mayor
     */
    public boolean valida(Mayor m) {
        for (Mayor mayor : getListaMayors()) {
            if (mayor.getCidade().equals(m.getCidade())) {
                return false;
            }
        }
        return true;
    }

    /**
     * Adiciona um mayor à lista
     *
     * @param m - Mayor
     */
    public void addMayor(Mayor m) {
        listaMayors.add(m);
    }

    /**
     * Atualiza o mayor
     *
     * @param outroMayor - Outro mayor
     */
    public void setMayorCidade(Mayor outroMayor) {
        for (Mayor m : listaMayors) {
            if (m.getCidade().equals(outroMayor.getCidade())) {
                m.setUtilizador(outroMayor.getUtilizador());
                m.setPontos(outroMayor.getPontos());
            }
        }
    }

}
