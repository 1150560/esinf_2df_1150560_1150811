/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 *
 * @author NOVO
 */
public class Utilizador implements Comparable<Utilizador> {

    /*
     * Nickname do utilizador
     */
    private String nickname;

    /*
     * Email do utilizador
     */
    private String email;

    /*
     * Lista de cidades do utilizador
     */
    private Map<Cidade, Integer> listaCidades;

    /**
     * Lista de CheckIn
     */
    private LinkedList listaCheckIn;

    /**
     * Pontos totais
     */
    private int pontosTotais;

    /*
     * Cidade atual
     */
    private Cidade cidadeAtual;

    /*
     * Lista de amigos do utilizador
     */
    private RegistoUtilizadores listaAmigos;

    /*
     * Nickname do utilizador por omissão
     */
    private static final String NICK_NAME_POR_OMISSAO = "Sem nick";

    /*
     * Email do utilizador por omissão
     */
    private static final String EMAIL_POR_OMISSAO = "Sem email";

    /*
     * Lista de cidades do utilizador por omissão
     */
    private final LinkedHashMap LISTA_CIDADES_POR_OMISSAO = new LinkedHashMap<>();

    /**
     * Lista de checkIn por omissao
     */
    private final LinkedList LISTA_CHECK_IN_POR_OMISSAO = new LinkedList<>();

    /**
     * Pontos totais por omissão
     */
    private static final int PONTOS_FINAIS_POR_OMISSAO = 0;

    /**
     * Cidade atual por omissao
     */
    private final Cidade CIDADE_ATUAL_POR_OMISSAO = null;

    /*
     * Lista de amigos do utilizador por omissão
     */
    private final RegistoUtilizadores LISTA_AMIGOS_POR_OMISSAO = new RegistoUtilizadores();

    /**
     * Constrói uma instância de Utilizador
     */
    public Utilizador() {
        this.nickname = NICK_NAME_POR_OMISSAO;
        this.email = EMAIL_POR_OMISSAO;
        this.listaCidades = LISTA_CIDADES_POR_OMISSAO;
        this.listaAmigos = LISTA_AMIGOS_POR_OMISSAO;
        this.cidadeAtual = CIDADE_ATUAL_POR_OMISSAO;
        this.listaCheckIn = LISTA_CHECK_IN_POR_OMISSAO;
    }

    /**
     * Constrói uma instância de Utilizador recebendo como parâmetro o nickname e o email
     *
     * @param nickname - nickname
     * @param email - email
     */
    public Utilizador(String nickname, String email) {
        setNickname(nickname);
        setEmail(email);
        this.listaCidades = LISTA_CIDADES_POR_OMISSAO;
        this.listaAmigos = LISTA_AMIGOS_POR_OMISSAO;
        this.cidadeAtual = CIDADE_ATUAL_POR_OMISSAO;
        this.listaCheckIn = LISTA_CHECK_IN_POR_OMISSAO;
    }

    /**
     * Constrói uma instância de Utilizador recebendo como parâmetro o nickname, o email, a lista de cidades e a lista de amigos
     *
     * @param nickname - nickname
     * @param email - email
     * @param lc - lista de cidades
     * @param la - lista de amigos
     */
    public Utilizador(String nickname, String email, LinkedHashMap lc, RegistoUtilizadores la) {
        setNickname(nickname);
        setEmail(email);
        this.listaCidades = lc;
        this.listaAmigos = new RegistoUtilizadores(la);
        this.cidadeAtual = CIDADE_ATUAL_POR_OMISSAO;
        this.listaCheckIn = LISTA_CHECK_IN_POR_OMISSAO;
    }

    /**
     * Constrói uma instância de Utilizador recebendo como parâmetro um outro utilizador
     *
     * @param outroUtilizador - outro utilizador
     */
    public Utilizador(Utilizador outroUtilizador) {
        setNickname(outroUtilizador.nickname);
        setEmail(outroUtilizador.email);
        this.listaCidades = outroUtilizador.listaCidades;
        this.listaAmigos = outroUtilizador.listaAmigos;
        this.cidadeAtual = outroUtilizador.cidadeAtual;
        this.listaCheckIn = outroUtilizador.listaCheckIn;
    }

    /**
     * Devolve o nickname
     *
     * @return the nickname
     */
    public String getNickname() {
        return nickname;
    }

    /**
     * Devolve o email
     *
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Devolve a lista de cidades
     *
     * @return the listaCidades
     */
    public Map<Cidade, Integer> getListaCidades() {
        return listaCidades;
    }

    /**
     * Devolve a lista de checkin
     *
     * @return the listaCheckIn
     */
    public LinkedList getListaCheckIn() {
        return listaCheckIn;
    }

    /**
     * Devolve o número de pontos totais
     *
     * @return the pontosTotais
     */
    public int getPontosTotais() {
        return pontosTotais;
    }

    /**
     * Devolve a cidade atual
     *
     * @return the cidadeAtual
     */
    public Cidade getCidadeAtual() {
        return cidadeAtual;
    }

    /**
     * Devolve a lista de amigos
     *
     * @return the listaAmigos
     */
    public RegistoUtilizadores getListaAmigos() {
        return listaAmigos;
    }

    /**
     * Modifica o nickname
     *
     * @param nickname - novo nickname
     */
    public void setNickname(String nickname) {
        if (nickname == null || nickname.trim().isEmpty()) {
            throw new IllegalArgumentException("Nome inválido!");
        } else {
            this.nickname = nickname;
        }
    }

    /**
     * Modifica o email
     *
     * @param email - novo email
     */
    public void setEmail(String email) {
        if (validaEmail(email)) {
            this.email = email;
        } else {
            throw new IllegalArgumentException("Email errado!");
        }
    }

    /**
     * Modifica a lista de cidades
     *
     * @param listaCidades - nova lista de cidades
     */
    public void setListaCidades(Map<Cidade, Integer> listaCidades) {
        this.listaCidades = listaCidades;
    }

    /**
     * Modifica a lista de check in
     *
     * @param listaCheckIn - lista de checkin
     */
    public void setListaCheckIn(LinkedList listaCheckIn) {
        this.listaCheckIn = listaCheckIn;
    }

    /**
     * Modifica o nº de pontos totais
     *
     * @param pontosTotais - novos pontos totais
     */
    public void setPontosTotais(int pontosTotais) {
        this.pontosTotais = pontosTotais;
    }

    /**
     * Modifica a cidade atual
     *
     * @param cidadeAtual - nova cidade atual
     */
    public void setCidadeAtual(Cidade cidadeAtual) {
        this.cidadeAtual = cidadeAtual;
    }

    /**
     * Modifica a lista de amigos
     *
     * @param listaAmigos - nova lista de amigos
     */
    public void setListaAmigos(RegistoUtilizadores listaAmigos) {
        this.listaAmigos = listaAmigos;
    }

    /**
     * Devolve a descrição textual de Utilizador
     *
     * @return descricao textual Utilizador
     */
    @Override
    public String toString() {
        return String.format("[%s]", nickname, email);
    }

    /**
     * Valida o email
     *
     * @param email - email
     */
    public boolean validaEmail(String email) {
        String ePattern = "^[_0-9A-Za-z-\\\\+]+(\\\\.[_0-9-A-Za-z]+)*@[a-zA-Z\\\\.]*";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }

    /**
     * Verifica se o utilizador recebido como parâmetro é igual ao atual
     *
     * @param u - Utilizador
     */
    public boolean equals(Utilizador u) {
        if (this == u) {
            return true;
        }
        if (u == null || getClass() != u.getClass()) {
            return false;
        }
        Utilizador outroUtilizador = (Utilizador) u;
        return email.equalsIgnoreCase(outroUtilizador.email) && nickname.equals(outroUtilizador.nickname);
    }

    /**
     * Devovle os pontos que utilizador tem na cidade
     *
     * @param nomeCidade - nome da cidade
     *
     * @return pontos
     */
    public int getPontosCidade(String nomeCidade) {
        for (Cidade c : listaCidades.keySet()) {
            if (c.getNome().trim().equalsIgnoreCase(nomeCidade)) {
                return listaCidades.get(c);
            }
        }
        return -1;
    }

    /**
     * Adiciona pontos à cidade que fez check in
     *
     * @param c - cidade
     * @param nPontos - pontos
     */
    public void addPontosCidade(Cidade c, int nPontos) {
        int pontosAntigos = 0;
        if (listaCidades.containsKey(c)) {
            pontosAntigos = listaCidades.get(c);
        }
        setPontosTotais(getPontosTotais() + nPontos);
        listaCidades.put(c, nPontos + pontosAntigos);
    }

    /**
     * Remove um amigo caso exista na lista de amigos
     *
     * @param u - Utilizador
     * @return true se remover o amigo
     */
    public boolean removerAmigo(Utilizador u) {
        int i = 0;
        for (Utilizador user : listaAmigos.getListaUtilizadores()) {
            if (user.equals(u)) {
                listaAmigos.getListaUtilizadores().remove(i);
                return true;
            }
            i++;
        }
        return false;
    }

    /**
     * Compare to, utilizadores
     *
     * @param t
     * @return
     */
    @Override
    public int compareTo(Utilizador t) {
        
        return this.hashCode() - t.hashCode();
    }
}
