/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Graph.Edge;
import Graph.Graph;
import Graph.GraphAlgorithm;
import Graph.Vertex;
import Matriz.AdjacencyMatrixGraph;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 *
 * @author NOVO
 */
public class RegistoUtilizadores {

    /**
     * A lista de utilizadores
     */
    private List<Utilizador> listaUtilizadores;

    /**
     * Constrói uma instância de RegistoUtilizadores recebendo como parâmetro uma lista de utilizadores
     *
     * @param listaUtilizadores - lista de utilizadores
     */
    public RegistoUtilizadores(List<Utilizador> listaUtilizadores) {
        this.listaUtilizadores = new LinkedList<>(listaUtilizadores);
    }

    /**
     * Constrói uma instância de RegistoUtilizadores recebendo como parâmetro um outro objeto RegistoUtilizadores
     *
     * @param outroRegistoUtilizadores - outro objeto RegistoUtilizadores
     */
    public RegistoUtilizadores(RegistoUtilizadores outroRegistoUtilizadores) {
        listaUtilizadores = outroRegistoUtilizadores.getListaUtilizadores();
    }

    /**
     * Constrói uma instância de RegistoUtilizadores
     */
    public RegistoUtilizadores() {
        this.listaUtilizadores = new LinkedList<Utilizador>();
    }

    /**
     * Devolve a lista de utilizadores
     *
     * @return the listaUtilizadores
     */
    public List<Utilizador> getListaUtilizadores() {
        return listaUtilizadores;
    }

    /**
     * Modifica a lista de utilizadores
     *
     * @param listaUtilizadores - nova lista de utilizadores
     */
    public void setListaUtilizadores(List<Utilizador> listaUtilizadores) {
        this.listaUtilizadores = new LinkedList<>(listaUtilizadores);
    }

    /**
     * Devolve a descrição textual de RegistoUtilizadores
     *
     * @return descricao textual RegistoUtilizadores
     */
    @Override
    public String toString() {
        return "A lista de utilizadores é:\n" + listaUtilizadoresToString();
    }

    /**
     * Modifica a forma como é apresentada a lista de utilizadores
     *
     * @return descricao textual RegistoUtilizadores
     */
    public String listaUtilizadoresToString() {
        StringBuilder s = new StringBuilder();
        for (Utilizador u : listaUtilizadores) {
            s.append(u);
            s.append("\n");
        }
        return s.toString();
    }

    /**
     * Cria um novo Utilizador
     *
     * @return utilizador
     */
    public Utilizador novoUtilizador() {
        Utilizador utilizador = new Utilizador();
        return utilizador;
    }

    /**
     * Regista o utilizador
     *
     * @param u - utilizador
     * @return
     */
    public boolean registaUtilizador(Utilizador u) {
        if (valida(u)) {
            addUtilizador(u);
            return true;
        }
        return false;
    }

    /**
     * Verifica se o utilizar já existe na lista de utilizadres
     *
     * @param u - utilizador
     * @return true se o utilizador nao existir na lista, false se existir
     */
    public boolean valida(Utilizador u) {
        for (Utilizador user : getListaUtilizadores()) {
            if (user.getNickname().equalsIgnoreCase(u.getNickname())
                    || user.getEmail().equalsIgnoreCase(u.getEmail())) {
                return false;
            }
        }
        return true;
    }

    /**
     * Adiciona um utilizador à lista
     *
     * @param u - Utilizador
     */
    public void addUtilizador(Utilizador u) {
        listaUtilizadores.add(u);
    }

    /**
     * Retorna o utilizador com o nickname
     *
     * @param nickname - nickname
     * @return utilizador
     */
    public Utilizador getUtilizador(String nickname) {
        if (isUtilizador(nickname)) {
            for (Utilizador u : listaUtilizadores) {
                if (u.getNickname().trim().equalsIgnoreCase(nickname)) {
                    return u;
                }
            }
        }
        return null;
    }

    /**
     * Veirfica se o utilizador com o nickname está na lista de utilizadores
     *
     * @param nickname - nickname
     * @return true se o utilizador existir na lista
     */
    public boolean isUtilizador(String nickname) {
        for (Utilizador u : listaUtilizadores) {
            if (u.getNickname().trim().equalsIgnoreCase(nickname)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Remove um utilizador caso o utilizador exista na lista de utilizadores
     *
     * @param u - Utilizador
     * @return return true se remover o utilizador
     */
    public boolean removerUtilizador(Utilizador u) {
        int i = 0;
        for (Utilizador user : listaUtilizadores) {
            if (user.equals(u)) {
                listaUtilizadores.remove(i);
                return true;
            }
            i++;
        }
        return false;
    }

    /**
     * Devolve a lista de amigos em que a cidade atual é c
     *
     * @param c - Cidade
     * @return listaAmigos
     */
    public LinkedList<Utilizador> getListaAmigosLocalizacao(Cidade c) {
        LinkedList<Utilizador> laLocalizacao = new LinkedList<>();
        for (Utilizador u : listaUtilizadores) {
            if (u.getCidadeAtual().equals(c)) {
                laLocalizacao.add(u);
            }
        }
        return laLocalizacao;
    }

    /**
     * Retorna um map ordenado por ordem decrescente de numero de amigos que cada utilizador tem
     *
     * @return map de Utilizadores/nAmigos
     */
    public Map<Utilizador, Integer> getListaUtilizadoresInfluentes() {
        Map<Utilizador, Integer> listaU = new LinkedHashMap<>();

        //Preenche a listaU em que a key é U, value nAmigos
        for (Utilizador u : listaUtilizadores) {
            listaU.put(u, u.getListaAmigos().getListaUtilizadores().size());
        }

        //Criacao de um array List de maps onde se vai ordenar por ordem decrescente de values
        List<Map.Entry<Utilizador, Integer>> values = new ArrayList<>(listaU.entrySet());
        Collections.sort(values, new Comparator<Map.Entry<Utilizador, Integer>>() {
            @Override
            public int compare(Map.Entry<Utilizador, Integer> u1,
                    Map.Entry<Utilizador, Integer> u2) {
                return u2.getValue() - u1.getValue();
            }
        });

        //Introduz no mno map os utilizadores e nAmigos por ordem decrescente
        Map<Utilizador, Integer> listaUFinal = new LinkedHashMap<>();
        int nInfluentes = 0;
        for (Entry<Utilizador, Integer> entry : values) {
            if (nInfluentes <= 5) {
                listaUFinal.put(entry.getKey(), entry.getValue());
            } else {
                break;
            }
            nInfluentes++;
        }
        return listaUFinal;
    }
}
